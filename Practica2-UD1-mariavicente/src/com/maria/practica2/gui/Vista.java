package com.maria.practica2.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.maria.practica2.base.Viaje;

import javax.swing.*;

public class Vista {
    /**
     * Dar de alta los atributos que necesitamos en la vista
     */
    private JPanel panel1;
    public JFrame frame;
    public JTextField nombreTxt;
    public JTextField destinoTxt;
    public JRadioButton apartamentoRadioButton;
    public JRadioButton hotelRadioButton;
    public JRadioButton pensionRadioButton;
    public DatePicker datePicker;
    public JTextField presupuestoTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public JTextField apellidoTxt;
    public JLabel fechaSalida;
    public JLabel todoLbl;
    public JTextField textField1;

    public DefaultListModel<Viaje> dlmDestino;

    /**
     * Creacion del constructor
     */
    public Vista(){
        frame = new JFrame("Agencia de Viajes");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    /**
     * Creamos metodo para que los componenetes se inicien
     */
    private void initComponents() {
        dlmDestino = new DefaultListModel<Viaje>();
        list1.setModel(dlmDestino);
    }


}
