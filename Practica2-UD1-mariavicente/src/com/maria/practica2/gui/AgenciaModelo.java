package com.maria.practica2.gui;

import com.maria.practica2.base.Apartamento;
import com.maria.practica2.base.Hotel;
import com.maria.practica2.base.Pension;
import com.maria.practica2.base.Viaje;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class AgenciaModelo {
    /**
     * Creacion del arraylist que necesitaremos
     */
    private ArrayList<Viaje> listaViaje;

    /**
     * creacion del constructor
     */
    public AgenciaModelo(){
        listaViaje = new ArrayList<Viaje>();
    }

    /**
     * Creamos metodo para que pueda obtener el viaje y nos devuelva la lista
     * @return
     */
    public ArrayList<Viaje> obtenerViaje(){
        return listaViaje;
    }

    /**
     * Dar de alta un hotel
     */
    public void altaHotel(String nombreCliente, String apellido, String destino, String presupuesto,
                          LocalDate fechaSalida, int estrellas){
        Hotel nuevoHotel = new Hotel(nombreCliente, apellido, destino, presupuesto, fechaSalida, estrellas);
        listaViaje.add(nuevoHotel);
    }

    /**
     * Dar de alta una Pension
     */
    public void altaPension(String nombreCliente, String apellido, String destino, String presupuesto,
                            LocalDate fechaSalida, int banoCompartido){
    Pension nuevaPesion = new Pension(nombreCliente, apellido, destino, presupuesto, fechaSalida, banoCompartido);
    listaViaje.add(nuevaPesion);
    }
    /**
     * Dar de alta un Apartamento
     */
    public void altaApartamento(String nombreCliente, String apellido, String destino, String presupuesto,
                                LocalDate fechaSalida, int numPersonas){
        Apartamento nuevoApartamento = new Apartamento(nombreCliente, apellido, destino, presupuesto, fechaSalida, numPersonas);
        listaViaje.add(nuevoApartamento);
    }

    /**
     * Comprobar si existe el nombre del cliente
     * @param apellidoCliente
     * @return
     */
    public boolean existeApellidoCliente(String apellidoCliente){
        for (Viaje unViaje: listaViaje){
            if(unViaje.getApellido().equals(apellidoCliente)){
                return true;
            }
        }
        return false;
    }

    /**
     * Exportar un fichero xml
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Viajes");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoViaje = null, nodoDatos = null;
        Text texto = null;

        for (Viaje unViaje : listaViaje) {
              if (unViaje instanceof Hotel) {
                  nodoViaje = documento.createElement("Hotel");
              }
              if (unViaje instanceof Pension){
                  nodoViaje = documento.createElement("Pension");
              }
              if (unViaje instanceof Apartamento){
                  nodoViaje = documento.createElement("Apartamento");
              }
            raiz.appendChild(nodoViaje);

            nodoDatos = documento.createElement("Nombre-Cliente");
            nodoViaje.appendChild(nodoDatos);

            texto = documento.createTextNode(unViaje.getNombreCliente());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Apellido-Cliente");
            nodoViaje.appendChild(nodoDatos);

            texto = documento.createTextNode(unViaje.getApellido());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Destino");
            nodoViaje.appendChild(nodoDatos);

            texto = documento.createTextNode(unViaje.getDestino());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Presupuesto-por-persona");
            nodoViaje.appendChild(nodoDatos);

            texto = documento.createTextNode(String.valueOf(unViaje.getPresupuesto()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Fecha-salida");
            nodoViaje.appendChild(nodoDatos);

            texto = documento.createTextNode(unViaje.getFechaSalida().toString());
            nodoDatos.appendChild(texto);

            if (unViaje instanceof Hotel) {
                nodoDatos = documento.createElement("Numero-estrellas");
                nodoViaje.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Hotel) unViaje).getEstrellas()));
                nodoDatos.appendChild(texto);
            }
            else if (unViaje instanceof Pension) {
                nodoDatos = documento.createElement("Baño-compartido");
                nodoViaje.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Pension) unViaje).getBanoCompartido()));
                nodoDatos.appendChild(texto);
            }
            else {
                nodoDatos = documento.createElement("Numero-personas");
                nodoViaje.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Apartamento) unViaje).getNumPersonas()));
                nodoDatos.appendChild(texto);
            }
        }

        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     * Importar un fichero xml
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaViaje = new ArrayList<Viaje>();
        Hotel nuevoHotel = null;
        Pension nuevaPension = null;
        Apartamento nuevoApartamento = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoViaje = (Element) listaElementos.item(i);

            if (nodoViaje.getTagName().equals("Hotel")) {
                nuevoHotel = new Hotel();
                nuevoHotel.setNombreCliente(nodoViaje.getChildNodes().item(0).getTextContent());
                nuevoHotel.setApellido(nodoViaje.getChildNodes().item(1).getTextContent());
                nuevoHotel.setDestino(nodoViaje.getChildNodes().item(2).getTextContent());
                nuevoHotel.setPresupuesto(nodoViaje.getChildNodes().item(3).getTextContent());
                nuevoHotel.setFechaSalida(LocalDate.parse(nodoViaje.getChildNodes().item(4).getTextContent()));
                nuevoHotel.setEstrellas(Integer.parseInt(nodoViaje.getChildNodes().item(5).getTextContent()));

                listaViaje.add(nuevoHotel);

            } else if(nodoViaje.getTagName().equals("Pension")){
                    nuevaPension = new Pension();
                    nuevaPension.setNombreCliente(nodoViaje.getChildNodes().item(0).getTextContent());
                    nuevaPension.setApellido(nodoViaje.getChildNodes().item(1).getTextContent());
                    nuevaPension.setDestino(nodoViaje.getChildNodes().item(2).getTextContent());
                    nuevaPension.setPresupuesto(nodoViaje.getChildNodes().item(3).getTextContent());
                    nuevaPension.setFechaSalida(LocalDate.parse(nodoViaje.getChildNodes().item(4).getTextContent()));
                    nuevaPension.setBanoCompartido(Integer.parseInt(nodoViaje.getChildNodes().item(5).getTextContent()));

                    listaViaje.add(nuevaPension);
                }

            else{
                if (nodoViaje.getTagName().equals("Apartamento")){
                    nuevoApartamento = new Apartamento();
                    nuevoApartamento.setNombreCliente(nodoViaje.getChildNodes().item(0).getTextContent());
                    nuevoApartamento.setApellido(nodoViaje.getChildNodes().item(1).getTextContent());
                    nuevoApartamento.setDestino(nodoViaje.getChildNodes().item(2).getTextContent());
                    nuevoApartamento.setPresupuesto(nodoViaje.getChildNodes().item(3).getTextContent());
                    nuevoApartamento.setFechaSalida(LocalDate.parse(nodoViaje.getChildNodes().item(4).getTextContent()));
                    nuevoApartamento.setNumPersonas(Integer.parseInt(nodoViaje.getChildNodes().item(5).getTextContent()));

                    listaViaje.add(nuevoApartamento);
                }
            }
        }
    }
}
