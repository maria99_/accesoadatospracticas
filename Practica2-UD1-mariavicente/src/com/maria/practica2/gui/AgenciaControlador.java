package com.maria.practica2.gui;

import com.maria.practica2.base.Apartamento;
import com.maria.practica2.base.Hotel;
import com.maria.practica2.base.Pension;
import com.maria.practica2.base.Viaje;
import com.maria.practica2.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

public class AgenciaControlador implements ActionListener, ListSelectionListener, WindowListener {
    /**
     * Creamos los metodos que necesitamos
     */
    private Vista vista;
    private AgenciaModelo modelo;
    private File ultimaRutaExportada;

    /**
     * Crear un constructor con metodos
     * @param vista
     * @param modelo
     */
    public AgenciaControlador(Vista vista, AgenciaModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }

    /**
     * Crear un actionPerformed con un switch para que haga las funciones que le indicamos
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre\nApellido\nDestino\nPresupuesto\nFecha");
                    vista.textField1.getText();
                    break;
                }

                if (modelo.existeApellidoCliente(vista.apellidoTxt.getText())) {
                    Util.mensajeError("Ya existe un cliente con ese apellido \n+" +
                            vista.apellidoTxt.getText());
                    break;
                }
                if (vista.hotelRadioButton.isSelected()) {
                    modelo.altaHotel(vista.nombreTxt.getText(), vista.apellidoTxt.getText(), vista.destinoTxt.getText(),
                            vista.presupuestoTxt.getText(), vista.datePicker.getDate(), Integer.parseInt(vista.textField1.getText()));
                }
               else if (vista.pensionRadioButton.isSelected()) {
                    modelo.altaPension(vista.nombreTxt.getText(), vista.apellidoTxt.getText(), vista.destinoTxt.getText(),
                            vista.presupuestoTxt.getText(), vista.datePicker.getDate(), Integer.parseInt(vista.textField1.getText()));
                }

               else if (vista.apartamentoRadioButton.isSelected()) {
                    modelo.altaApartamento(vista.nombreTxt.getText(), vista.apellidoTxt.getText(), vista.destinoTxt.getText(),
                           vista.presupuestoTxt.getText(), vista.datePicker.getDate(), Integer.parseInt(vista.textField1.getText()));
                }

                limpiarCampos();
                refrescar();
                break;

            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;

            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Hotel":
                vista.todoLbl.setText("Estrellas");
                break;

            case "Pension":
                vista.todoLbl.setText("Baño-compartido");
                break;

            case "Apartamento":
                vista.todoLbl.setText("Num-personas");
                break;
        }
    }

    /**
     * Metodo para que vea los campos vacios
     * @return true
     */
    private boolean hayCamposVacios(){
        if(vista.textField1.getText().isEmpty() ||
                vista.nombreTxt.getText().isEmpty() ||
                vista.apellidoTxt.getText().isEmpty() ||
                vista.destinoTxt.getText().isEmpty() ||
                vista.presupuestoTxt.getText().isEmpty() ||
                vista.datePicker.getText().isEmpty()){
            return true;
        }
        return false;
    }

    /**
     * Metodo para que nos limpie los campos
     */
    private void limpiarCampos(){
        vista.textField1.setText(null);
        vista.nombreTxt.setText(null);
        vista.apellidoTxt.setText(null);
        vista.destinoTxt.setText(null);
        vista.datePicker.setText(null);
        vista.presupuestoTxt.setText(null);
        vista.nombreTxt.requestFocus();
    }

    /**
     * Para que refresque el codigo
     */
    private void refrescar(){
        vista.dlmDestino.clear();
        for(Viaje unViaje: modelo.obtenerViaje()){
            vista.dlmDestino.addElement((unViaje));
        }
    }

    /**
     * Para que aparezca los botones y lor radio buton
     * @param listener
     */
    private void addActionListener(ActionListener listener){
        vista.apartamentoRadioButton.addActionListener(listener);
        vista.hotelRadioButton.addActionListener(listener);
        vista.pensionRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
    }

    /**
     * Cerrar la ventana de windows
     * @param listener
     */
    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    /**
     * Para que aparezca la lista
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * Cargar los datos
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("viaje.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Actualizamos la ruta de la configuracion
     * @param ultimaRutaExportada
     */
    private void actualizarConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Guardamos la ruta de la configuracion
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath()); //Clave y ruta
        configuracion.store(new PrintWriter("viaje.conf"), "Datos configuracion viajes");
    }

    /**
     * Actualizar los cambios
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Viaje viajeSeleccionado = (Viaje) vista.list1.getSelectedValue();
            vista.nombreTxt.setText((viajeSeleccionado.getNombreCliente()));
            vista.apellidoTxt.setText(viajeSeleccionado.getApellido());
            vista.destinoTxt.setText(viajeSeleccionado.getDestino());
            vista.presupuestoTxt.setText(viajeSeleccionado.getPresupuesto());
            vista.datePicker.setDate(viajeSeleccionado.getFechaSalida());

            if (viajeSeleccionado instanceof Hotel) {
                vista.hotelRadioButton.doClick();
                vista.textField1.setText(String.valueOf(((Hotel) viajeSeleccionado).getEstrellas()));
            }
           else if (viajeSeleccionado instanceof Pension) {
                vista.pensionRadioButton.doClick();
                vista.textField1.setText(String.valueOf(((Pension) viajeSeleccionado).getBanoCompartido()));
            }
            else {
                vista.apartamentoRadioButton.doClick();
                vista.textField1.setText(String.valueOf(((Apartamento) viajeSeleccionado).getNumPersonas()));
            }
        }
    }

    /**
     * Cerrar la pestaña de windows
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int respuesta = Util.mensajeConfirmacion("¿Deseas salir de la aplicacion?", "Salir");
        if (respuesta == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowClosed(WindowEvent e) {

    }


    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowIconified(WindowEvent e) {

    }

    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowActivated(WindowEvent e) {

    }

    /**
     * Estos metodos no los utilizamos
     * @param e
     */
    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}
