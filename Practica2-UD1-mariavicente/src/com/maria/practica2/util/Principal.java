package com.maria.practica2.util;

import com.maria.practica2.gui.AgenciaControlador;
import com.maria.practica2.gui.AgenciaModelo;
import com.maria.practica2.gui.Vista;

public class Principal {

    /**
     * Metodo main para que se ejecute
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        AgenciaModelo modelo = new AgenciaModelo();
        AgenciaControlador controlador = new AgenciaControlador(vista, modelo);
        
    }
}
