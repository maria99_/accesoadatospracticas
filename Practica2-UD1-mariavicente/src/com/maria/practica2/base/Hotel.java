package com.maria.practica2.base;

import java.time.LocalDate;

public class Hotel extends Viaje{
    /**
     * Creamos un atributo que necesitaremos en esta clase
     */
    private int estrellas;

    /**
     * Crear constuctor con los atributos de la clase hija
     */
    public Hotel(){
        super();
    }

    /**
     * Creamos constructor
     * @param nombreCliente
     * @param apellido
     * @param destino
     * @param presupuesto
     * @param fechaSalida
     * @param estrellas
     */
    public Hotel(String nombreCliente, String apellido, String destino, String presupuesto,
                 LocalDate fechaSalida, int estrellas) {
        super(nombreCliente, apellido, destino, presupuesto, fechaSalida);
        this.estrellas = estrellas;
    }

    /**
     * Creacion de geters y setteres
     * @return
     */
    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    /**
     * Creacion del toString
     * @return
     */
    public String toString() {
        return "Hotel: " + getNombreCliente() + " " + getApellido() + " " + getDestino() + " " + getPresupuesto()
                + getFechaSalida() + " " +  getEstrellas();
    }

}
