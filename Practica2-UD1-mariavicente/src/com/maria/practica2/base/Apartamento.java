package com.maria.practica2.base;

import java.time.LocalDate;

public class Apartamento extends Viaje{
    /**
     * Creamos un atributo que necesitaremos en esta clase
     */
    private int numPersonas;

    /**
     * Crear constuctor con los atributos de la clase hija
     */
    public Apartamento(){
        super();
    }

    /**
     * Creamos constructor
     * @param nombreCliente
     * @param apellido
     * @param destino
     * @param presupuesto
     * @param fechSalida
     * @param numPersonas
     */
    public Apartamento(String nombreCliente, String apellido, String destino, String presupuesto, LocalDate fechSalida, int numPersonas) {
      super(nombreCliente, apellido, destino, presupuesto, fechSalida);
      this.numPersonas = numPersonas;
    }

    /**
     * Creacion de geters y setteres
     * @return
     */
    public int getNumPersonas() {
        return numPersonas;
    }

    public void setNumPersonas(int numPersonas) {
        this.numPersonas = numPersonas;
    }

    /**
     * Creacion del toString
     * @return
     */
    @Override
    public String toString() {
        return "Apartamento: " + getNombreCliente() + " " + getApellido() + " " + getDestino() + " " + getNumPersonas() + " " + getPresupuesto()
                + getFechaSalida();
    }
}
