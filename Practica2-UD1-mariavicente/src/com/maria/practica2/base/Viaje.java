package com.maria.practica2.base;

import java.time.LocalDate;

public abstract class Viaje {
    /**
     * Creacion de atributos
     */
    private String nombreCliente;
    private String apellido;
    private String destino;
    private String presupuesto;
    private LocalDate fechaSalida;

    /**
     * Creamos un constructor
     * @param nombreCliente
     * @param apellido
     * @param destino
     * @param presupuesto
     * @param fechaSalida
     * @param apellido
     */
    public Viaje(String nombreCliente, String apellido, String destino, String presupuesto, LocalDate fechaSalida) {
        this.nombreCliente = nombreCliente;
        this.destino = destino;
        this.presupuesto = presupuesto;
        this.fechaSalida = fechaSalida;
        this.apellido = apellido;
    }

    /**
     * Creamos un constructor vacio
     */
    public Viaje(){

    }
    /**
     * Creacion de getters y setters
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(String presupuesto) {
        this.presupuesto = presupuesto;
    }

    public LocalDate getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(LocalDate fechaVacaciones) {
        this.fechaSalida = fechaSalida;
    }

    public String getApellido() {
        return apellido;
    }
}
