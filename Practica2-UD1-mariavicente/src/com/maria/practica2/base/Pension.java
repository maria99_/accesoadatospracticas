package com.maria.practica2.base;

import java.time.LocalDate;

public class Pension extends Viaje{
    /**
     * Creamos un atributo que necesitaremos en esta clase
     */
    private int banoCompartido;

    /**
     * Crear constuctor con los atributos de la clase hija
     */
    public Pension(){
        super();
    }

    /**
     * Crear constructor
     * @param nombreCliente
     * @param apellido
     * @param destino
     * @param presupuesto
     * @param fechaSalida
     * @param banoCompartido
     */
    public Pension(String nombreCliente, String apellido, String destino, String presupuesto, LocalDate fechaSalida, int banoCompartido) {
        super(nombreCliente, apellido, destino, presupuesto, fechaSalida);
        this.banoCompartido = banoCompartido;
    }

    /**
     * Crear getters y setters
     * @return
     */
    public int getBanoCompartido() {
        return banoCompartido;
    }

    public void setBanoCompartido(int banoCompartido) {
        this.banoCompartido = banoCompartido;
    }

    /**
     * Crear metodo toString
     * @return
     */
    public String toString() {
        return "Pension: " + getNombreCliente() + " "  + getApellido() + " " + getDestino() + " " + getPresupuesto()
                + getFechaSalida() + " " + getBanoCompartido();
    }
}
