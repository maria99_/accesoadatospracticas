package com.mariava.agencia;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "destino", schema = "agenciamaria", catalog = "")

public class Destino {
    private int id;
    private String codigo;
    private String nombreDestino;
    private String descripcion;
    private Date fecha;
    private List<ContrataCliente> contrataclientes;
    private List<Excursion> excursiones;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre_destino")
    public String getNombreDestino() {
        return nombreDestino;
    }

    public void setNombreDestino(String nombreDestino) {
        this.nombreDestino = nombreDestino;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destino destino = (Destino) o;
        return Objects.equals(id, destino.id) && Objects.equals(codigo, destino.codigo) && Objects.equals(nombreDestino, destino.nombreDestino) && Objects.equals(descripcion, destino.descripcion) && Objects.equals(fecha, destino.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, nombreDestino, descripcion, fecha);
    }

    @OneToMany(mappedBy = "destino")
    public List<ContrataCliente> getContrataclientes() {
        return contrataclientes;
    }

    public void setContrataclientes(List<ContrataCliente> contrataclientes) {
        this.contrataclientes = contrataclientes;
    }

    @ManyToMany
    @JoinTable(name = "destino_excursion", catalog = "", schema = "agenciamaria", joinColumns = @JoinColumn(name = "id_excursion",
            referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id_destino", referencedColumnName = "id", nullable = false))
    public List<Excursion> getExcursiones() {
        return excursiones;
    }

    public void setExcursiones(List<Excursion> excursiones) {
        this.excursiones = excursiones;
    }
}
