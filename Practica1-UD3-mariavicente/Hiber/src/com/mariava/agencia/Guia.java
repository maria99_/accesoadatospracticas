package com.mariava.agencia;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "guia", schema = "agenciamaria", catalog = "")

public class Guia {


    private int id;
    private String nombreGuia;
    private Date fechaContratacion;
    private Double precioTotal;
    private Cliente clientes;
    private List<DetalleGuia> detalleguia;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "nombre_guia")
    public String getNombreGuia() {
        return nombreGuia;
    }

    public void setNombreGuia(String nombreGuia) {
        this.nombreGuia = nombreGuia;
    }

    @Basic
    @Column(name = "fecha_contratacion")
    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Basic
    @Column(name = "precio_total")
    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guia guia = (Guia) o;
        return Objects.equals(id, guia.id) && Objects.equals(nombreGuia, guia.nombreGuia) && Objects.equals(fechaContratacion, guia.fechaContratacion) && Objects.equals(precioTotal, guia.precioTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreGuia, fechaContratacion, precioTotal);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }

    @OneToMany(mappedBy = "guia")
    public List<DetalleGuia> getDetalleguia() {
        return detalleguia;
    }

    public void setDetalleguia(List<DetalleGuia> detalleguia) {
        this.detalleguia = detalleguia;
    }
}
