CREATE database IF NOT EXISTS AGENCIA;
--
USE AGENCIA ;
-- -----------------------------------------------------
-- Table `mydb`.`agencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS agencia (
id_agencia INT PRIMARY KEY AUTO_INCREMENT,
direccion VARCHAR(45),
telefono INT);
-- -----------------------------------------------------
-- Table `mydb`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cliente (
 id_cliente INT PRIMARY KEY AUTO_INCREMENT,
 id_agencia INT,
 nombre VARCHAR(45),
 apellido VARCHAR(45) ,
 fecha_nacimiento DATE ,
 telefono INT ,
 dni VARCHAR(45),
 direccion VARCHAR(45) ,
 codigo_postal INT);
 --

-- -----------------------------------------------------
-- Table `mydb`.`vuelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS vuelo (
  id_vuelo INT PRIMARY KEY AUTO_INCREMENT,
  numerovuelo INT ,
  compañia  VARCHAR(45) ,
  origen VARCHAR(45) ,
  destino VARCHAR(45) ,
  plazas INT ,
  fecha DATE ,
  precio FLOAT);
-- -----------------------------------------------------
-- Table `mydb`.`cliente_vuelo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cliente_vuelo (
  idcliente_vuelo INT PRIMARY KEY AUTO_INCREMENT,
  id_cliente INT,
  idvuelo INT);
 
-- -----------------------------------------------------
-- Table `mydb`.`aeropuerto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS aeropuerto (
  id_aeropuerto INT PRIMARY KEY AUTO_INCREMENT,
  descripcion VARCHAR(45));
-- -----------------------------------------------------
-- Table `mydb`.`vuelo_aeropuerto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS vuelo_aeropuerto (
  idvuelo_aeropuerto INT PRIMARY KEY AUTO_INCREMENT,
  id_vuelo INT,
  id_aeropuerto INT);
-- -----------------------------------------------------
-- Table `mydb`.`hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS hotel (
  id_hotel INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(45),
  direccion VARCHAR(45) ,
  ciudad VARCHAR(45) ,
  telefono VARCHAR(45) ,
  plazas_disponibles INT);

-- -----------------------------------------------------
-- Table `mydb`.`cliente_hotel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS cliente_hotel (
  idcliente_hotel  INT PRIMARY KEY AUTO_INCREMENT,
  id_cliente INT ,
  id_hotel INT);
  
  
-- -----------------------------------------------------
-- Table `mydb`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS categoria (
  idcategoria INT PRIMARY KEY AUTO_INCREMENT,
  iva FLOAT ,
  descripcion VARCHAR(45));

-- -----------------------------------------------------
-- Table `mydb`.`hotel_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS hotel_categoria (
  idhotel_categoria INT PRIMARY KEY AUTO_INCREMENT,
  id_hotel INT ,
  id_categoria INT);
 -- 
alter table cliente
add foreign key (id_agencia) references agencia (id_agencia);

alter table cliente_vuelo
add foreign key (id_cliente) references cliente (id_cliente);

alter table cliente_hotel
add foreign key (id_cliente) references cliente (id_cliente);

alter table vuelo_aeropuerto
add foreign key (id_aeropuerto) references aeropuerto (id_aeropuerto);

alter table cliente_vuelo
add foreign key (idvuelo) references vuelo (id_vuelo);

alter table hotel_categoria
add foreign key (id_categoria) references categoria (idcategoria);

alter table hotel_categoria 
add foreign key (id_hotel) references hotel (id_hotel);

alter table cliente_hotel  
add foreign key (id_hotel) references hotel (id_hotel);

alter table vuelo_aeropuerto
add foreign key (id_vuelo) references vuelo (id_vuelo);


