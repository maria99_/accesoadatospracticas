package com.mariava.agencia;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private Long id;
    private int idCliente;
    private String nombre;
    private String apellido;
    private Date fechaNacimiento;
    private int telefono;
    private String dni;
    private String direccion;
    private int codigoPostal;
    private Agencia agencias;
    private List<ClienteVuelo> coge;
    private List<ClienteHotel> coger;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_cliente")
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "codigo_postal")
    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return idCliente == cliente.idCliente && telefono == cliente.telefono && codigoPostal == cliente.codigoPostal && Objects.equals(nombre, cliente.nombre) && Objects.equals(apellido, cliente.apellido) && Objects.equals(fechaNacimiento, cliente.fechaNacimiento) && Objects.equals(dni, cliente.dni) && Objects.equals(direccion, cliente.direccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCliente, nombre, apellido, fechaNacimiento, telefono, dni, direccion, codigoPostal);
    }

    @ManyToOne
    @JoinColumn(name = "id_agencia", referencedColumnName = "id_agencia")
    public Agencia getAgencias() {
        return agencias;
    }

    public void setAgencias(Agencia agencias) {
        this.agencias = agencias;
    }

    @OneToMany(mappedBy = "clientes")
    public List<ClienteVuelo> getCoge() {
        return coge;
    }

    public void setCoge(List<ClienteVuelo> coge) {
        this.coge = coge;
    }

    @OneToMany(mappedBy = "clientes")
    public List<ClienteHotel> getCoger() {
        return coger;
    }

    public void setCoger(List<ClienteHotel> coger) {
        this.coger = coger;
    }
}
