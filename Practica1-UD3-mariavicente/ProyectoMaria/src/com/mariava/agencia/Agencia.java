package com.mariava.agencia;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Agencia {
    private Long id;
    private int idAgencia;
    private String direccion;
    private int telefono;
    private List<Cliente> cliente;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_agencia")
    public int getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agencia agencia = (Agencia) o;
        return idAgencia == agencia.idAgencia && telefono == agencia.telefono && Objects.equals(direccion, agencia.direccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAgencia, direccion, telefono);
    }

    @OneToMany(mappedBy = "agencias")
    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }
}
