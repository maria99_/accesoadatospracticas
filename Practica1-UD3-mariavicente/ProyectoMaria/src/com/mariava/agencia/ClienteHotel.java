package com.mariava.agencia;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cliente_hotel", schema = "agenciaviajes")
public class ClienteHotel {
    private Long id;
    private int idclienteHotel;
    private Cliente clientes;
    private Hotel hotel;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "idcliente_hotel")
    public int getIdclienteHotel() {
        return idclienteHotel;
    }

    public void setIdclienteHotel(int idclienteHotel) {
        this.idclienteHotel = idclienteHotel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteHotel that = (ClienteHotel) o;
        return idclienteHotel == that.idclienteHotel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idclienteHotel);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }

    @ManyToOne
    @JoinColumn(name = "id_hotel", referencedColumnName = "id_hotel")
    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
