package com.mariava.agencia;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Categoria {
    private Long id;
    private int idcategoria;
    private Double iva;
    private String descripcion;
    private Hotel hotel;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "idcategoria")
    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    @Basic
    @Column(name = "iva")
    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return idcategoria == categoria.idcategoria && Objects.equals(iva, categoria.iva) && Objects.equals(descripcion, categoria.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcategoria, iva, descripcion);
    }

    @ManyToOne
    @JoinColumn(name = "idcategoria", referencedColumnName = "id_hotel", nullable = false)
    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
