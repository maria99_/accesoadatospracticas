package com.mariava.agencia;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "hotel_categoria", schema = "agenciaviajes")
public class HotelCategoria {
    private Long id;
    private int idhotelCategoria;
    private List<Hotel> hoteles;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "idhotel_categoria")
    public int getIdhotelCategoria() {
        return idhotelCategoria;
    }

    public void setIdhotelCategoria(int idhotelCategoria) {
        this.idhotelCategoria = idhotelCategoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotelCategoria that = (HotelCategoria) o;
        return idhotelCategoria == that.idhotelCategoria;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhotelCategoria);
    }

    @ManyToMany(mappedBy = "cat")
    public List<Hotel> getHoteles() {
        return hoteles;
    }

    public void setHoteles(List<Hotel> hoteles) {
        this.hoteles = hoteles;
    }
}
