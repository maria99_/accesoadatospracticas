package com.mariava.agencia;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cliente_vuelo", schema = "agenciaviajes")
public class ClienteVuelo {
    private Long id;
    private int idclienteVuelo;
    private Cliente clientes;
    private Vuelo vuelo;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "idcliente_vuelo")
    public int getIdclienteVuelo() {
        return idclienteVuelo;
    }

    public void setIdclienteVuelo(int idclienteVuelo) {
        this.idclienteVuelo = idclienteVuelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteVuelo that = (ClienteVuelo) o;
        return idclienteVuelo == that.idclienteVuelo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idclienteVuelo);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }

    @ManyToOne
    @JoinColumn(name = "idvuelo", referencedColumnName = "id_vuelo")
    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }
}
