package com.mariava.agencia;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Aeropuerto {
    private Long id;
    private int idAeropuerto;
    private String descripcion;
    private Vuelo vuelo;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_aeropuerto")
    public int getIdAeropuerto() {
        return idAeropuerto;
    }

    public void setIdAeropuerto(int idAeropuerto) {
        this.idAeropuerto = idAeropuerto;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aeropuerto that = (Aeropuerto) o;
        return idAeropuerto == that.idAeropuerto && Objects.equals(descripcion, that.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAeropuerto, descripcion);
    }

    @ManyToOne
    @JoinColumn(name = "id_aeropuerto", referencedColumnName = "id_vuelo", nullable = false)
    public Vuelo getVuelo() {
        return vuelo;
    }

    public void setVuelo(Vuelo vuelo) {
        this.vuelo = vuelo;
    }
}
