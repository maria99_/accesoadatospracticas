package com.mariava.agencia;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Vuelo {
    private Long id;
    private int idVuelo;
    private int numerovuelo;
    private String compañia;
    private String origen;
    private String destino;
    private int plazas;
    private Date fecha;
    private Double precio;
    private List<ClienteVuelo> clientes;
    private List<Aeropuerto> aeropuerto;
    private List<VueloAeropuerto> aeropuertos;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_vuelo")
    public int getIdVuelo() {
        return idVuelo;
    }

    public void setIdVuelo(int idVuelo) {
        this.idVuelo = idVuelo;
    }

    @Basic
    @Column(name = "numerovuelo")
    public int getNumerovuelo() {
        return numerovuelo;
    }

    public void setNumerovuelo(int numerovuelo) {
        this.numerovuelo = numerovuelo;
    }

    @Basic
    @Column(name = "compañia")
    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "destino")
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Basic
    @Column(name = "plazas")
    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vuelo vuelo = (Vuelo) o;
        return idVuelo == vuelo.idVuelo && numerovuelo == vuelo.numerovuelo && plazas == vuelo.plazas && Objects.equals(compañia, vuelo.compañia) && Objects.equals(origen, vuelo.origen) && Objects.equals(destino, vuelo.destino) && Objects.equals(fecha, vuelo.fecha) && Objects.equals(precio, vuelo.precio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVuelo, numerovuelo, compañia, origen, destino, plazas, fecha, precio);
    }

    @OneToMany(mappedBy = "vuelo")
    public List<ClienteVuelo> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteVuelo> clientes) {
        this.clientes = clientes;
    }

    @OneToMany(mappedBy = "vuelo")
    public List<Aeropuerto> getAeropuerto() {
        return aeropuerto;
    }

    public void setAeropuerto(List<Aeropuerto> aeropuerto) {
        this.aeropuerto = aeropuerto;
    }

    @ManyToMany
    @JoinTable(name = "vuelo_aeropuerto", schema = "agenciaviajes", joinColumns = @JoinColumn(name = "id_vuelo", referencedColumnName = "id_vuelo"), inverseJoinColumns = @JoinColumn(name = "id_aeropuerto", referencedColumnName = "id_aeropuerto"))
    public List<VueloAeropuerto> getAeropuertos() {
        return aeropuertos;
    }

    public void setAeropuertos(List<VueloAeropuerto> aeropuertos) {
        this.aeropuertos = aeropuertos;
    }
}
