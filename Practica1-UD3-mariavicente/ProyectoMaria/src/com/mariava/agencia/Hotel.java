package com.mariava.agencia;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Hotel {
    private Long id;
    private int idHotel;
    private String nombre;
    private String direccion;
    private String ciudad;
    private String telefono;
    private int plazasDisponibles;
    private List<ClienteHotel> clientes;
    private List<Categoria> categoria;
    private List<HotelCategoria> cat;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_hotel")
    public int getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(int idHotel) {
        this.idHotel = idHotel;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "plazas_disponibles")
    public int getPlazasDisponibles() {
        return plazasDisponibles;
    }

    public void setPlazasDisponibles(int plazasDisponibles) {
        this.plazasDisponibles = plazasDisponibles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return idHotel == hotel.idHotel && plazasDisponibles == hotel.plazasDisponibles && Objects.equals(nombre, hotel.nombre) && Objects.equals(direccion, hotel.direccion) && Objects.equals(ciudad, hotel.ciudad) && Objects.equals(telefono, hotel.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idHotel, nombre, direccion, ciudad, telefono, plazasDisponibles);
    }

    @OneToMany(mappedBy = "hotel")
    public List<ClienteHotel> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteHotel> clientes) {
        this.clientes = clientes;
    }

    @OneToMany(mappedBy = "hotel")
    public List<Categoria> getCategoria() {
        return categoria;
    }

    public void setCategoria(List<Categoria> categoria) {
        this.categoria = categoria;
    }

    @ManyToMany
    @JoinTable(name = "hotel_categoria", schema = "agenciaviajes", joinColumns = @JoinColumn(name = "id_hotel", referencedColumnName = "id_hotel"), inverseJoinColumns = @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria"))
    public List<HotelCategoria> getCat() {
        return cat;
    }

    public void setCat(List<HotelCategoria> cat) {
        this.cat = cat;
    }
}
