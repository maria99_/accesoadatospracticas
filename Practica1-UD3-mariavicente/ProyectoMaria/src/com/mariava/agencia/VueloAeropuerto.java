package com.mariava.agencia;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "vuelo_aeropuerto", schema = "agenciaviajes")
public class VueloAeropuerto {
    private Long id;
    private int idvueloAeropuerto;
    private List<Vuelo> vuelos;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "idvuelo_aeropuerto")
    public int getIdvueloAeropuerto() {
        return idvueloAeropuerto;
    }

    public void setIdvueloAeropuerto(int idvueloAeropuerto) {
        this.idvueloAeropuerto = idvueloAeropuerto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VueloAeropuerto that = (VueloAeropuerto) o;
        return idvueloAeropuerto == that.idvueloAeropuerto;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idvueloAeropuerto);
    }

    @ManyToMany(mappedBy = "aeropuertos")
    public List<Vuelo> getVuelos() {
        return vuelos;
    }

    public void setVuelos(List<Vuelo> vuelos) {
        this.vuelos = vuelos;
    }
}
