CREATE DATABASE IF NOT EXISTS agenciamaria; 
--
USE agenciamaria;
--
 CREATE TABLE IF NOT EXISTS cliente  (  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,  
 dni VARCHAR(10) NOT NULL UNIQUE,  
 email VARCHAR(50),  
 nombre VARCHAR(50),  
 direccion VARCHAR(50),  
 telefono VARCHAR(20) ); 
 --
 CREATE TABLE IF NOT EXISTS destino(  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,  
 codigo VARCHAR(50) UNIQUE NOT NULL,  
 nombre_destino VARCHAR(50) NOT NULL,  
 descripcion VARCHAR(500),  
 fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP() );
 --
 CREATE TABLE IF NOT EXISTS guia (  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT, 
 nombre_guia varchar(50) NOT NULL,
 fecha_contratacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),  
 precio_total FLOAT DEFAULT 0,   
 id_cliente INT UNSIGNED NOT NULL REFERENCES cliente ); 
 --
 CREATE TABLE IF NOT EXISTS excursion (  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,  
 codigo VARCHAR(10) UNIQUE NOT NULL,  
 nombre_excursion VARCHAR(50) NOT NULL,  
 descripcion VARCHAR(500),  
 precio FLOAT DEFAULT 0 ); 
 --
 CREATE TABLE IF NOT EXISTS cliente_destino (  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 id_cliente INT REFERENCES cleinte,  
 id_destino INT REFERENCES destino,  
 fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP());
 --
 CREATE TABLE IF NOT EXISTS destino_excursion (  
 id_destino INT UNSIGNED REFERENCES destino,  
 id_excursion INT UNSIGNED REFERENCES excursion,  
 PRIMARY KEY (id_destino, id_excursion) );
 --
 CREATE TABLE IF NOT EXISTS guia_excursion (  
 id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
 id_guia INT REFERENCES guia,  
 id_excursion INT REFERENCES excursion,  
 cantidad INTEGER DEFAULT 0,  
 precio FLOAT DEFAULT 0); 
 
 