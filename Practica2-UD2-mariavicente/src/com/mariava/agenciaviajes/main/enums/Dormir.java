package com.mariava.agenciaviajes.main.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena JComoboBox comboGenero de la vista.
 * Representan los sitios donde puedes dormir.
 */
public enum Dormir {
   HOTEL("Hotel"),
    PENSION("Pension"),
    CAMPING("Camping"),
    APARTAMENTO("Apartamento"),
    HOSTAL("Hostal");

    private String valor;

    Dormir(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}
