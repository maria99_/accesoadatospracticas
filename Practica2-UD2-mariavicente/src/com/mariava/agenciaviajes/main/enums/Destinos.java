package com.mariava.agenciaviajes.main.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena JComoboBox comboGenero de la vista.
 * Representan los destinos a los que puedes ir.
 */
public enum Destinos {
    MADRID("Madrid"),
    ZARAGOZA("Zaragoza"),
    TERUEL("Teruel"),
    HUESCA("Huesca"),
    BUDAPEST("Budapest"),
    POLONIA("Cracovia"),
    PAISESBAJOS("Amsterdam"),
    GRECIA("Santorini"),
    EXTREMADURA("Caceres"),
    FRANCIA("Paris"),
    HORLANDO("Horlando");

    private String valor;

    Destinos(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}
