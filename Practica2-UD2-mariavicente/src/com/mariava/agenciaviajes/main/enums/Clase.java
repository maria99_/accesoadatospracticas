package com.mariava.agenciaviajes.main.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena JComoboBox comboGenero de la vista.
 * Representan la clase.
 */
public enum Clase {
    PRIMERACLASE("Primera Clase"),
    SEGUNDACLASE("Segunda Clase"),
    TERCERACLASE("Tercera Clase");

    private String valor;

    Clase(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}
