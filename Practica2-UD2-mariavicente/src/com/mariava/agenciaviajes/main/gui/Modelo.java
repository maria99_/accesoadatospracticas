package com.mariava.agenciaviajes.main.gui;

import com.mariava.agenciaviajes.main.util.Util;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase e inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     * Generacion de getters y setters
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Creamos una tabla para el cliente, por si anteriormente no la teniamos creada y
     * necesitamos crear una nueva
     *
     * @throws SQLException excepcion
     */
    public void crearTablaCliente() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/agencia2", "root", "");

        String sentenciaSql = "call crearTablaCliente()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Creamos una tabla para vuelo, por si anteriormente no la teniamos creada y
     * necesitamos crear una nueva
     *
     * @throws SQLException excepcion
     */
    public void crearTablaVuelo() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/agencia2", "root", "");

        String sentenciaSql = "call crearTablaVuelo()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Creamos una tabla para alojamiento, por si anteriormente no la teniamos creada y
     * necesitamos crear una nueva
     *
     * @throws SQLException excepcion
     */
    public void crearTablaAlojamiento() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/agencia2", "root", "");

        String sentenciaSql = "call crearTablaAlojamiento()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/agencia2",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo que nos permite leer un fichero para que aparezcan los datos
     * @return devuelve el dato
     * @throws IOException una excepcion de error
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Desconectar la base de datos
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * Insertar a la tabla de vuelo los siguientes atributos.
     * @param nvuelo numero de vuelo
     * @param clase clase que esta en un combobox
     * @param fechaIda fecha a la que vas
     * @param fechaVuelta fecha cuando vuelves
     * @param puertaEntrada puerta por donde embarcas
     * @param asiento donde te vas a sentar
     * @param destino desde donde quieres ir
     * @param donde a donde quieres llegar
     * @param precio lo que cuesta el vuelo
     */
    void insertarVuelo(int nvuelo, String clase, LocalDate fechaIda, LocalDate fechaVuelta,
                       String puertaEntrada, String asiento, String destino, String donde, float precio) {
        String sentenciaSql = "INSERT INTO vuelo (numerovuelo, clase, fechaida, fechavuelta, " +
                "puertaentrada, asiento, destino, donde, precio) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, nvuelo);
            sentencia.setString(2, clase);
            sentencia.setDate(3, Date.valueOf(fechaIda));
            sentencia.setDate(4,Date.valueOf(fechaVuelta));
            sentencia.setString(5, puertaEntrada);
            sentencia.setString(6, asiento);
            sentencia.setString(7, destino);
            sentencia.setString(8, donde);
            sentencia.setFloat(9, precio);
            sentencia.executeUpdate();

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que inseerta a la tabla alojamiento los datos que introducimos
     * @param cAlojamiento codigo e alojamiento
     * @param dormir donde dormir que esta en un combobox
     * @param nhab numero de habitaciones que quieres
     * @param npersonas numero de personas que van a dormir
     * @param parking si necesitas parking o no
     * @param entradaAlojamiento dia y hora a la que entras
     * @param salidaAlojamiento dia y hora a la que sales
     */
    void insertarAlojamiento(int cAlojamiento, String dormir, int nhab, int npersonas, String parking,
                             LocalDate entradaAlojamiento, LocalDate salidaAlojamiento, float precio) {
        String sentenciaSql = "INSERT INTO alojamiento (codigoalojamiento, dormir, numerohabitaciones, numeropersonas, " +
                "parking, entrada, salida, precio) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, cAlojamiento);
            sentencia.setString(2, dormir);
            sentencia.setInt(3, nhab);
            sentencia.setInt(4, npersonas);
            sentencia.setString(5, parking);
            sentencia.setDate(6, Date.valueOf(entradaAlojamiento));
            sentencia.setDate(7, Date.valueOf(salidaAlojamiento));
            sentencia.setFloat(8, precio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Insertar en la tabla los datos del cliente
     * @param nombre del cliente
     * @param apellido del cliente
     * @param dni del cliente
     * @param telefono del cliente
     * @param direccion del cliente
     * @param cp del cliente
     * @param fechaNacimiento del cliente
     * @param vuelo combobox donde van los datos del vuelo
     * @param alojamiento combobox donde van los datos del alojamiento
     */
    void insertarCliente(String nombre, String apellido, String dni, int telefono, String direccion, int cp, LocalDate fechaNacimiento,
                         String vuelo, String alojamiento) {
        String sentenciaSql = "INSERT INTO cliente (nombre, apellidos, dni, telefono, direccion, codigopostal, " +
                "fechanacimiento, idvuelo, idalojamiento) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idvuelo = Integer.valueOf(vuelo.split(" ")[0]);
        int idalojamiento = Integer.valueOf(alojamiento.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setInt(4, telefono);
            sentencia.setString(5, direccion);
            sentencia.setInt(6, cp);
            sentencia.setDate(7, Date.valueOf(fechaNacimiento));
            sentencia.setInt(8, idvuelo);
            sentencia.setInt(9, idalojamiento);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificar la clase Vuelo
     * @param numeroVuelo cambiar el numero
     * @param clase cambiar la clase que habias seleccionado antes
     * @param fechaIda cambiar la fecha que habias selecciondo antes
     * @param fechaVuelta cambiar la fecha que habias selecciondo antes
     * @param puertaEntrada cambiar la puerta de entrada que habias selecciondo antes
     * @param asiento cambiar el asiento que habias selecciondo antes
     * @param destino cambiar el destino que habias selecciondo antes
     * @param donde cambiar el lugar que habias selecciondo antes
     * @param idvuelo saber que dato es el que hay
     * @param precio lo que cuesta el vuelo
     */
    void modificarVuelo(int numeroVuelo, String clase, LocalDate fechaIda, LocalDate fechaVuelta,
                        String puertaEntrada, String asiento, String destino, String donde, float precio, int idvuelo){

        String sentenciaSql = "UPDATE vuelo SET numerovuelo = ?, clase = ?, fechaida = ?, fechavuelta = ?, puertaentrada = ?, " +
                "asiento = ?, destino = ?, donde = ?, precio = ? WHERE idvuelo = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, numeroVuelo);
            sentencia.setString(2, clase);
            sentencia.setDate(3, Date.valueOf(fechaIda));
            sentencia.setDate(4, Date.valueOf(fechaVuelta));
            sentencia.setString(3, puertaEntrada);
            sentencia.setString(4, asiento);
            sentencia.setString(5, destino);
            sentencia.setString(6, donde);
            sentencia.setFloat(7, precio);
            sentencia.setInt(8, idvuelo);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Modificar la tabla de alojamiento
     * @param cAlojamiento del alojamiento
     * @param dormir cambiar donde dormir
     * @param nhab cambiar las habitaciones
     * @param npersonas cambiar las personas que van al viaje
     * @param parking cambiar si quiere parking o no
     * @param entradaAlojamiento cambiar la fehca de entrada del alojamiento
     * @param salidaAlojamiento cambiar la fehca de salida del alojamiento
     * @param idalojamiento sabeer que numero tiene el alojamiento
     * @param precio lo que cuesta el alojamiento
     */
    void modificarAlojamiento(int cAlojamiento, String dormir, int nhab, int npersonas, String parking,
                              LocalDate entradaAlojamiento, LocalDate salidaAlojamiento, float precio, int idalojamiento){

        String sentenciaSql = "UPDATE alojamiento SET codigoalojamiento = ?, dormir = ?, numerohabitaciones = ?, numeropersonas = ?, " +
                "parking = ?, entrada = ?, salida = ?, precio= ? WHERE idalojamiento = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, cAlojamiento);
            sentencia.setString(2, dormir);
            sentencia.setInt(3, nhab);
            sentencia.setInt(4, npersonas);
            sentencia.setString(5, parking);
            sentencia.setDate(6, Date.valueOf(entradaAlojamiento));
            sentencia.setDate(7, Date.valueOf(salidaAlojamiento));
            sentencia.setFloat(8, precio);
            sentencia.setInt(9, idalojamiento);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo para modificar el cliente
     * @param nombre del cliente
     * @param apellido del cliente
     * @param dni del cliente
     * @param telefono del cliente
     * @param direccion del cliente
     * @param cp del cliente
     * @param fechaNacimiento del cliente
     * @param vuelo al que quiere ir
     * @param alojamiento al que quiere ir
     * @param idcliente campo para saber que codigo tiene el cliente
     */
    void modificarCliente(String nombre, String apellido, String dni, int telefono, String direccion, int cp, LocalDate fechaNacimiento,
                          String vuelo, String alojamiento, int idcliente) {

        String sentenciaSql = "UPDATE cliente SET nombre = ?, apellidos = ?, dni = ?, telefono = ?, direccion = ?, codigopostal = ?" +
                ", fechanacimiento = ?, idvuelo = ?, idalojamiento = ? WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        int idvuelo = Integer.valueOf(vuelo.split(" ")[0]);
        int idalojamiento = Integer.valueOf(alojamiento.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setInt(4, telefono);
            sentencia.setString(5, direccion);
            sentencia.setInt(6, cp);
            sentencia.setDate(7, Date.valueOf(fechaNacimiento));
            sentencia.setInt(8, idvuelo);
            sentencia.setInt(9, idalojamiento);
            sentencia.setInt(10, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo para eliminar un vuelo
     * @param idvuelo id del vuelo
     */
    void borrarVuelo(int idvuelo) {
        String sentenciaSql = "DELETE FROM vuelo WHERE idvuelo = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idvuelo);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo para eliminar un vuelo
     * @param idalojamiento id del alojamiento
     */
    void borrarAlojamiento(int idalojamiento) {
        String sentenciaSql = "DELETE FROM alojamiento WHERE idalojamiento = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idalojamiento);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo elimina un cliente
     * @param idcliente id del cliente
     */
    void borrarCliente(int idcliente) {
        String sentenciaSql = "DELETE FROM cliente WHERE idcliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idcliente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo devuelve los vuelos guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarVuelo() throws SQLException {
        String sentenciaSql = "SELECT concat(idvuelo) as 'ID', concat(numerovuelo) as 'Numero Vuelo', concat(clase) as 'Clase', " +
                "concat(fechaida) as 'Fecha Ida', concat(fechavuelta) as 'Fecha Vuelta', concat(puertaentrada) as 'Puerta de Entrada', " +
                "concat(asiento) as 'Asiento', concat(destino) as 'Destino',concat(donde) as 'Donde', concat(precio) as 'Precio' FROM vuelo";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve los alojamientos guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarAlojamiento() throws SQLException {
        String sentenciaSql = "SELECT concat(idalojamiento) as 'ID', concat(codigoalojamiento) as 'Codigo Alojamiento'," +
                " concat(dormir) as 'Dormir', concat(numerohabitaciones) as 'Numero habitaciones', " +
                "concat(numeropersonas) as 'Numero personas', concat(parking) as 'Parking', concat(entrada) as 'Entrada alojamiento'," +
                "concat(salida) as 'Salida alojamiento', concat(precio) as 'Precio' FROM alojamiento";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve todos los clientes guardados en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarCliente() throws SQLException {

        String sentenciaSql = "SELECT concat(b.idcliente) as 'ID', concat(b.nombre) as 'Nombre', concat(b.apellidos) as 'Apellidos', concat(b.dni) as 'DNI', " +
                "concat(b.telefono) as 'Telefono', concat(b.direccion) as 'Direccion', concat(b.codigopostal) as 'Codigo Postal', " +
                "concat(b.fechanacimiento) as 'Fecha de nacimiento', " +
                "concat(e.idvuelo, '-', e.numerovuelo) as 'Vuelo', " +
                "concat(a.idalojamiento, '-', codigoalojamiento) as 'Alojamiento'" +
                " FROM cliente as b " +
                "inner join vuelo as e on e.idvuelo = b.idvuelo inner join " +
                "alojamiento as a on a.idalojamiento = b.idalojamiento";

        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    /**
     * Coomprueba si el cliente ya esta en la base de datos
     * @param dni del cliente
     * @return true si ya existe
     */
    public boolean clienteYaExiste(String dni) {
        String clienteConsulta = "SELECT existeDni(?)";
        PreparedStatement function;
        boolean isbnExists = false;
        try {
            function = conexion.prepareStatement(clienteConsulta);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            isbnExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isbnExists;
    }

    /**
     * Este metodo comprueba si numero de vuelo ya existe en la base de datos
     * @param numeroVuelo numero del vuelo
     * @return true si ya existe
     */
    public boolean numeroVueloYaExiste(String numeroVuelo) {
        String numeroVueloConsulta = "SELECT existeNumeroVuelo(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(numeroVueloConsulta);
            function.setString(1, numeroVuelo);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Este metodo comprueba si el codigo de alojamiento existe
     * @param codigoAlojamiento comprobar si existe
     * @return true si existe
     */
    public boolean alojamientoYaExiste(String codigoAlojamiento) {
        String alojamientoConsulta = "SELECT existeAlojamiento(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(alojamientoConsulta);
            function.setString(1, codigoAlojamiento);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Crear metodo para buscr un cliente por DNI
     */
    public void buscarClienteDNI() {
        String buscarCliente = "SELECT dni FROM cliente";
        PreparedStatement function;
        boolean encontrado = false;
        try {
            String codBuscar = "";
            codBuscar = JOptionPane.showInputDialog("Introduzca un DNI:");

            function = conexion.prepareStatement(buscarCliente);

            ResultSet rs = function.executeQuery();
            while (rs.next()){
                if (codBuscar.equals(rs.getObject("Dni"))){
                    encontrado = true;
                    Util.showInfoAlert("DNI existente");
                    break;
                }
            }
            if(encontrado == false){
                Util.showErrorAlert("No se encontro el DNI");
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Buscar vuelo por codigo de vuelo
     */
    public void buscarVuelo() {
        String buscarV = "SELECT numerovuelo FROM vuelo";
        PreparedStatement function;
        boolean encontrado = false;
        try {
            String codBuscar = "";
            codBuscar = JOptionPane.showInputDialog("Introduzca un Numero de vuelo:");

            function = conexion.prepareStatement(buscarV);

            ResultSet rs = function.executeQuery();
            while (rs.next()){
                if (codBuscar.equals(rs.getObject("numerovuelo"))){
                    encontrado = true;
                    Util.showInfoAlert("Nuemro de vuelo existente");
                    break;
                }
            }
            if(encontrado == false){
                Util.showErrorAlert("No se encontro el numero de vuelo");
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Buscar alojamiento por codigo de alojamiento
     */
    public void buscarAlojamiento() {
        String buscarA = "SELECT codigoalojamiento FROM alojamiento";
        PreparedStatement function;
        boolean encontrado = false;
        try {
            String codBuscar = "";
            codBuscar = JOptionPane.showInputDialog("Introduzca un codigo de alojamiento:");

            function = conexion.prepareStatement(buscarA);

            ResultSet rs = function.executeQuery();
            while (rs.next()){
                if (codBuscar.equals(rs.getObject("codigoalojamiento"))){
                    encontrado = true;
                    Util.showInfoAlert("Codigo de alojamiento existente");
                    break;
                }
            }
            if(encontrado == false){
                Util.showErrorAlert("No se encontro el codigo de alojamiento");
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
