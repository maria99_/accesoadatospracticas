package com.mariava.agenciaviajes.main.gui;

import com.mariava.agenciaviajes.main.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {
    /**
     * Crear atributos de la clase
     */
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * Metodo donde metemos todos los metodos que tiene que ver con refrescar (actualizar la tabla)
     */
    private void refrescarTodo() {
        refrescarVuelo();
        refrescarAlojamiento();
        refrescarCliente();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnClienteNuevo.addActionListener(listener);
        vista.btnAlojamientoAnadir.addActionListener(listener);
        vista.btnVueloAnadir.addActionListener(listener);
        vista.btnClienteEliminar.addActionListener(listener);
        vista.btnAlojamientoEliminar.addActionListener(listener);
        vista.btnVueloEliminar.addActionListener(listener);
        vista.btnClienteModificar.addActionListener(listener);
        vista.btnAlojamientoModificar.addActionListener(listener);
        vista.btnVueloModificar.addActionListener(listener);
        vista.btnClienteBuscar.addActionListener(listener);
        vista.btnAlojamientoBuscar.addActionListener(listener);
        vista.btnVueloBuscar.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e evento de una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()){
            if (e.getSource().equals(vista.vueloTabla.getSelectionModel())) {
                int row = vista.vueloTabla.getSelectedRow();
                vista.txtVueloNumero.setText(String.valueOf(vista.vueloTabla.getValueAt(row, 1)));
                vista.comboBoxVueloClase.setSelectedItem(String.valueOf(vista.vueloTabla.getValueAt(row, 2)));
                vista.dateVueloIda.setDate((Date.valueOf(String.valueOf(vista.vueloTabla.getValueAt(row, 3)))).toLocalDate());
                vista.dateVueloVuelta.setDate((Date.valueOf(String.valueOf(vista.vueloTabla.getValueAt(row, 4)))).toLocalDate());
                vista.txtVueloPuerta.setText(String.valueOf(vista.vueloTabla.getValueAt(row, 5)));
                vista.txtVueloAsiento.setText(String.valueOf(vista.vueloTabla.getValueAt(row, 6)));
                vista.comboBoxVueloDestino.setSelectedItem(String.valueOf(vista.vueloTabla.getValueAt(row, 7)));
                vista.comboBoxVueloDonde.setSelectedItem(String.valueOf(vista.vueloTabla.getValueAt(row, 8)));
                vista.txtVueloPrecio.setText(String.valueOf(vista.vueloTabla.getValueAt(row, 9)));

            } else if (e.getSource().equals(vista.alojamientoTabla.getSelectionModel())) {
                int row = vista.alojamientoTabla.getSelectedRow();
                vista.txtAlojamientoCodigo.setText(String.valueOf(vista.alojamientoTabla.getValueAt(row, 1)));
                vista.comboBoxAlojamientoDormir.setSelectedItem(String.valueOf(vista.vueloTabla.getValueAt(row, 2)));
                vista.txtAlojamientoHabitaciones.setText(String.valueOf(vista.alojamientoTabla.getValueAt(row, 3)));
                vista.txtAlojamientoPersonas.setText(String.valueOf(vista.alojamientoTabla.getValueAt(row, 4)));
                vista.txtAlojamientoParking.setText(String.valueOf(vista.alojamientoTabla.getValueAt(row, 5)));
                vista.entradaAlojamiento.setDate((Date.valueOf(String.valueOf(vista.alojamientoTabla.getValueAt(row, 6)))).toLocalDate());
                vista.salidaAlojamiento.setDate((Date.valueOf(String.valueOf(vista.alojamientoTabla.getValueAt(row, 7)))).toLocalDate());
                vista.txtAlojamientoPrecio.setText(String.valueOf(vista.alojamientoTabla.getValueAt(row, 8)));

            } else if (e.getSource().equals(vista.tablaClientes.getSelectionModel())) {
                int row = vista.tablaClientes.getSelectedRow();
                vista.txtClienteNombre.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 1)));
                vista.txtClienteApellido.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 2)));
                vista.txtClienteDni.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 3)));
                vista.txtClienteTelefono.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 4)));
                vista.txtClienteDireccion.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 5)));
                vista.txtClienteCP.setText(String.valueOf(vista.tablaClientes.getValueAt(row, 6)));
                vista.datePickerFecha.setDate((Date.valueOf(String.valueOf(vista.tablaClientes.getValueAt(row, 7)))).toLocalDate());
                vista.comboVuelo.setSelectedItem(String.valueOf(vista.tablaClientes.getValueAt(row, 8)));
                vista.comboAlojamiento.setSelectedItem(String.valueOf(vista.tablaClientes.getValueAt(row, 9)));

            } else if (e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.vueloTabla.getSelectionModel())) {
                    borrarCamposVuelo();
                } else if (e.getSource().equals(vista.alojamientoTabla.getSelectionModel())) {
                    borrarCamposAlojamiento();
                } else if (e.getSource().equals(vista.tablaClientes.getSelectionModel())) {
                    borrarCamposCliente();
                }
            }
        }
    }

    /**
     * Diferentes opciones en un switch para elegir dependiendo de la copcion que cojas una opcion u otra
     * @param e llamo asi la variable para ir actualizando durante la ejecucion
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;

            case "Desconectar":
                modelo.desconectar();
                vista.etiquetaEstado.setText("Desconectar");
                break;

            case "Salir":
                System.exit(0);
                break;

            case "abrirOpciones":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;

            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;

            case "crearTablaCliente":
                try{
                    modelo.crearTablaCliente();
                    vista.etiquetaEstado.setText("Tabla Clientes creada");
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "crearTablaVuelo":
                try{
                    modelo.crearTablaVuelo();
                    vista.etiquetaEstado.setText("Tabla Vuelo creada");
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "crearTablaAlojamiento":
                try{
                    modelo.crearTablaAlojamiento();
                    vista.etiquetaEstado.setText("Tabla Alojamiento creada");
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "anadirCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaClientes.clearSelection();
                    } else if (modelo.clienteYaExiste(vista.txtClienteDni.getText())) {
                        Util.showErrorAlert("Ese DNI ya existe.\nIntroduce un cliente diferente");
                        vista.tablaClientes.clearSelection();
                    } else {
                        modelo.insertarCliente(
                                vista.txtClienteNombre.getText(),
                                vista.txtClienteApellido.getText(),
                                vista.txtClienteDni.getText(),
                                Integer.valueOf(vista.txtClienteTelefono.getText()),
                                vista.txtClienteDireccion.getText(),
                                Integer.valueOf(vista.txtClienteCP.getText()),
                                vista.datePickerFecha.getDate(),
                                String.valueOf(vista.comboVuelo.getSelectedItem()),
                                String.valueOf(vista.comboAlojamiento.getSelectedItem()));
                        vista.etiquetaEstado.setText("Cliente Añadido");
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaClientes.clearSelection();
                }
                borrarCamposCliente();
                refrescarCliente();
            }
            break;

            case "modificarCliente": {
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaClientes.clearSelection();
                    } else {
                        modelo.modificarCliente(
                                vista.txtClienteNombre.getText(),
                                vista.txtClienteApellido.getText(),
                                vista.txtClienteDni.getText(),
                                Integer.valueOf(vista.txtClienteTelefono.getText()),
                                vista.txtClienteDireccion.getText(),
                                Integer.valueOf(vista.txtClienteCP.getText()),
                                vista.datePickerFecha.getDate(),
                                String.valueOf(vista.comboVuelo.getSelectedItem()),
                                String.valueOf(vista.comboAlojamiento.getSelectedItem()),
                                Integer.parseInt((String) vista.tablaClientes.getValueAt(vista.tablaClientes.getSelectedRow(), 0)));
                        vista.etiquetaEstado.setText("Cliente Modificado");
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaClientes.clearSelection();
                }
                borrarCamposCliente();
                refrescarCliente();
            }
            break;

            case "eliminarCliente":
                modelo.borrarCliente(Integer.parseInt((String) vista.tablaClientes.getValueAt(vista.tablaClientes.getSelectedRow(), 0)));
                borrarCamposCliente();
                refrescarCliente();
                vista.etiquetaEstado.setText("Clente Eliminado");
                break;

            case "anadirVuelo": {
                try {
                    if (comprobarVueloVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.vueloTabla.clearSelection();
                    } else if (modelo.numeroVueloYaExiste(vista.txtVueloNumero.getText())) {
                        Util.showErrorAlert("Ese numero de vuelo ya existe.\nIntroduce un numero de vuelo diferente");
                        vista.vueloTabla.clearSelection();
                    } else {
                        modelo.insertarVuelo(
                                Integer.parseInt(vista.txtVueloNumero.getText()),
                                String.valueOf(vista.comboBoxVueloClase.getSelectedItem()),
                                vista.dateVueloIda.getDate(),
                                vista.dateVueloVuelta.getDate(),
                                vista.txtVueloPuerta.getText(),
                                vista.txtVueloAsiento.getText(),
                                String.valueOf(vista.comboBoxVueloDestino.getSelectedItem()),
                                String.valueOf(vista.comboBoxVueloDonde.getSelectedItem()),
                                Float.parseFloat(vista.txtVueloPrecio.getText()));
                        vista.etiquetaEstado.setText("Vuelo Añadido");
                        refrescarVuelo();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.vueloTabla.clearSelection();
                }
                borrarCamposVuelo();
            }
            break;

            case "modificarVuelo": {
                try {
                    if (comprobarVueloVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.vueloTabla.clearSelection();
                    } else {
                        modelo.modificarVuelo(
                                Integer.parseInt(vista.txtVueloNumero.getText()),
                                String.valueOf(vista.comboBoxVueloClase.getSelectedItem()),
                                vista.dateVueloIda.getDate(),
                                vista.dateVueloVuelta.getDate(),
                                vista.txtVueloPuerta.getText(),
                                vista.txtVueloAsiento.getText(),
                                String.valueOf(vista.comboBoxVueloDestino.getSelectedItem()),
                                String.valueOf(vista.comboBoxVueloDonde.getSelectedItem()),
                                Float.parseFloat(vista.txtVueloPrecio.getText()),
                                Integer.parseInt((String) vista.vueloTabla.getValueAt(vista.vueloTabla.getSelectedRow(), 0)));
                        vista.etiquetaEstado.setText("Vuelo Modificado");
                        refrescarVuelo();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.vueloTabla.clearSelection();
                }
                borrarCamposVuelo();
            }
            break;

            case "eliminarVuelo":
                modelo.borrarVuelo(Integer.parseInt((String) vista.vueloTabla.getValueAt(vista.vueloTabla.getSelectedRow(), 0)));
                borrarCamposVuelo();
                refrescarVuelo();
                vista.etiquetaEstado.setText("Vuelo Eliminado");
                break;

            case "anadirAlojamiento": {
                try {
                    if (comprobarAlojamientoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.alojamientoTabla.clearSelection();
                    } else if (modelo.alojamientoYaExiste(vista.txtAlojamientoCodigo.getText())) {
                        Util.showErrorAlert("Ese codigo de alojamiento ya existe.\nIntroduce un codigo de alojamiento diferente.");
                        vista.alojamientoTabla.clearSelection();
                    } else {
                        modelo.insertarAlojamiento(
                                Integer.parseInt(vista.txtAlojamientoCodigo.getText()),
                                String.valueOf(vista.comboBoxAlojamientoDormir.getSelectedItem()),
                                Integer.parseInt(vista.txtAlojamientoHabitaciones.getText()),
                                Integer.parseInt(vista.txtAlojamientoPersonas.getText()),
                                vista.txtAlojamientoParking.getText(),
                                vista.entradaAlojamiento.getDate(),
                                vista.salidaAlojamiento.getDate(),
                                Float.parseFloat(vista.txtAlojamientoPrecio.getText()));
                        vista.etiquetaEstado.setText("Alojamiento Añadido");
                        refrescarAlojamiento();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.alojamientoTabla.clearSelection();
                }
                borrarCamposAlojamiento();
            }
            break;
            case "modificarAlojamiento": {
                try {
                    if (comprobarAlojamientoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.alojamientoTabla.clearSelection();
                    } else {
                        modelo.modificarAlojamiento(
                                Integer.parseInt(vista.txtAlojamientoCodigo.getText()),
                                String.valueOf(vista.comboBoxAlojamientoDormir.getSelectedItem()),
                                Integer.parseInt(vista.txtAlojamientoHabitaciones.getText()),
                                Integer.parseInt(vista.txtAlojamientoPersonas.getText()),
                                vista.txtAlojamientoParking.getText(),
                                vista.entradaAlojamiento.getDate(),
                                vista.salidaAlojamiento.getDate(),
                                Float.parseFloat(vista.txtAlojamientoPrecio.getText()),
                                Integer.parseInt((String) vista.vueloTabla.getValueAt(vista.vueloTabla.getSelectedRow(), 0)));
                        vista.etiquetaEstado.setText("Alojamiento Modificado");
                        refrescarAlojamiento();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.alojamientoTabla.clearSelection();
                }
                borrarCamposAlojamiento();
                }
                break;

            case "eliminarAlojamiento":
                modelo.borrarAlojamiento(Integer.parseInt((String)vista.alojamientoTabla.getValueAt(vista.alojamientoTabla.getSelectedRow(), 0)));
                borrarCamposAlojamiento();
                refrescarAlojamiento();
                vista.etiquetaEstado.setText("Alojamiento Eliminado");
                break;

            case "buscarCliente":
                modelo.buscarClienteDNI();
                vista.etiquetaEstado.setText("Buscando Cliente");
                break;

            case "buscarVuelo":
                modelo.buscarVuelo();
                vista.etiquetaEstado.setText("Buscando Vuelo");
                break;

            case "buscarAlojamiento":
                modelo.buscarAlojamiento();
                vista.etiquetaEstado.setText("Buscando Alojamiento");
                break;

            case "guardarOpcionesConectar":
                Util.showInfoAlert("Configuracion guardada");
                break;
        }
    }

    /**
     * Vacía los campos de la tabla de cliente
     */
    private void borrarCamposCliente() {
        vista.txtClienteNombre.setText("");
        vista.txtClienteApellido.setText("");
        vista.txtClienteDni.setText("");
        vista.txtClienteTelefono.setText("");
        vista.txtClienteDireccion.setText("");
        vista.txtClienteCP.setText("");
        vista.datePickerFecha.setText("");
        vista.comboVuelo.setSelectedIndex(-1);
        vista.comboAlojamiento.setSelectedIndex(-1);
    }

    /**
     * Vacía los campos de la tabla de alojamiento
     */
    private void borrarCamposAlojamiento() {
        vista.txtAlojamientoCodigo.setText("");
        vista.comboBoxAlojamientoDormir.setSelectedIndex(-1);
        vista.txtAlojamientoHabitaciones.setText("");
        vista.txtAlojamientoPersonas.setText("");
        vista.txtAlojamientoParking.setText("");
        vista.entradaAlojamiento.setText("");
        vista.salidaAlojamiento.setText("");
        vista.txtAlojamientoPrecio.setText("");
    }

    /**
     * Vacía los campos de la tabla de vuelo
     */
    private void borrarCamposVuelo() {
        vista.txtVueloNumero.setText("");
        vista.comboBoxVueloClase.setSelectedIndex(-1);
        vista.dateVueloIda.setText("");
        vista.dateVueloVuelta.setText("");
        vista.txtVueloPuerta.setText("");
        vista.txtVueloAsiento.setText("");
        vista.comboBoxVueloDestino.setSelectedIndex(-1);
        vista.comboBoxVueloDonde.setSelectedIndex(-1);
        vista.txtVueloPrecio.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir un vuelo estén vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarVueloVacia() {
        return  vista.txtVueloNumero.getText().isEmpty() ||
                vista.comboBoxVueloClase.getSelectedIndex() == -1 ||
                vista.dateVueloIda.getText().isEmpty() ||
                vista.dateVueloVuelta.getText().isEmpty() ||
                vista.txtVueloPuerta.getText().isEmpty() ||
                vista.txtVueloAsiento.getText().isEmpty() ||
                vista.comboBoxVueloDonde.getSelectedIndex() == -1 ||
                vista.comboBoxVueloDestino.getSelectedIndex() == -1 ||
                vista.txtVueloPrecio.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un alojamiento estén vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarAlojamientoVacio() {
        return  vista.txtAlojamientoCodigo.getText().isEmpty() ||
                vista.comboBoxAlojamientoDormir.getSelectedIndex() == -1 ||
                vista.txtAlojamientoHabitaciones.getText().isEmpty() ||
                vista.txtAlojamientoPersonas.getText().isEmpty() ||
                vista.txtAlojamientoParking.getText().isEmpty() ||
                vista.entradaAlojamiento.getText().isEmpty() ||
                vista.salidaAlojamiento.getText().isEmpty() ||
                vista.txtAlojamientoPrecio.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un cliente estén vacíos
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarClienteVacio() {
        return  vista.txtClienteNombre.getText().isEmpty() ||
                vista.txtClienteApellido.getText().isEmpty() ||
                vista.txtClienteDni.getText().isEmpty() ||
                vista.txtClienteTelefono.getText().isEmpty() ||
                vista.txtClienteDireccion.getText().isEmpty() ||
                vista.txtClienteCP.getText().isEmpty() ||
                vista.datePickerFecha.getText().isEmpty() ||
                vista.comboVuelo.getSelectedIndex() == -1 ||
                vista.comboAlojamiento.getSelectedIndex() == -1;
    }

    /**
     * Añade acciones cuando la ventana se cierra.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    /**
     * Actualiza los clientes que se ven en la lista y los combobox
     */
    private void refrescarCliente() {
        try {
            vista.tablaClientes.setModel(construirTableModelCliente(modelo.consultarCliente()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construir la tabla cliente con un vector
     * @param rs llamada a la variable
     * @return dtmCliente una excepcion
     * @throws SQLException una excepcion de error
     */
    private DefaultTableModel construirTableModelCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // Nombres de la columna
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmCliente.setDataVector(data, columnNames);

        return vista.dtmCliente;
    }

    /**
     * Actualiza los alojamientos que se ven en la lista y los combobox
     */
    private void refrescarAlojamiento() {
        try {
            vista.alojamientoTabla.setModel(construirTableModeloAlojamiento(modelo.consultarAlojamiento()));
            vista.comboAlojamiento.removeAllItems();
            for(int i = 0; i < vista.dtmAlojamiento.getRowCount(); i++) {
                vista.comboAlojamiento.addItem(vista.dtmAlojamiento.getValueAt(i, 0)+" - "+
                        vista.dtmAlojamiento.getValueAt(i, 2)+", "+vista.dtmAlojamiento.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construir la tabla alojamiento con un vector
     * @param rs llamada a la variable
     * @return dtmAlojamiento una excepcion
     * @throws SQLException una excepcion de error
     */
    private DefaultTableModel construirTableModeloAlojamiento(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // Nombre de las columnas
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmAlojamiento.setDataVector(data, columnNames);

        return vista.dtmAlojamiento;

    }

    /**
     * Actualiza los vuelos que se ven en la lista y los combobox
     */
    private void refrescarVuelo() {
        try {
            vista.vueloTabla.setModel(construirTableModelVuelo(modelo.consultarVuelo()));
            vista.comboVuelo.removeAllItems();
            for(int i = 0; i < vista.dtmVuelo.getRowCount(); i++) {
                vista.comboVuelo.addItem(vista.dtmVuelo.getValueAt(i, 0)+" - "+
                        vista.dtmVuelo.getValueAt(i, 2)+", "+vista.dtmVuelo.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construir la tabla vuelo con un vector
     * @param rs llamada a la variable
     * @return dtmVuelo una excepcion
     * @throws SQLException una excepcion de error
     */
    private DefaultTableModel construirTableModelVuelo(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        //Nombres de las columnas
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmVuelo.setDataVector(data, columnNames);

        return vista.dtmVuelo;

    }

    /**
     * Metodo el cual le pasamos un vector
     * @param rs llamada a la variable
     * @param columnCount cuenta las columnas
     * @param data llamada al vector
     * @throws SQLException una excepcion de error
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }


    /*LISTENERS IPLEMENTOS NO UTILIZADOS*/
    private void addItemListeners(Controlador controlador) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

}
