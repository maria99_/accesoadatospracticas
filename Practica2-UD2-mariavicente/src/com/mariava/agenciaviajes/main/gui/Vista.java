package com.mariava.agenciaviajes.main.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.mariava.agenciaviajes.main.enums.Clase;
import com.mariava.agenciaviajes.main.enums.Destinos;
import com.mariava.agenciaviajes.main.enums.Dormir;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{

    private final static String TITULOFRAME = "Agencia";
    private JPanel panel1;
    private JTabbedPane tabbedPane1;

    /*CLIENTE*/
    JButton btnClienteNuevo;
    JButton btnClienteModificar;
    JButton btnClienteEliminar;
    JButton btnClienteBuscar;
    JTextField txtClienteNombre;
    JTextField txtClienteApellido;
    JTextField txtClienteDni;
    JTextField txtClienteTelefono;
    JTextField txtClienteDireccion;
    JTextField txtClienteCP;
    DatePicker datePickerFecha;
    JComboBox<String> comboVuelo;
    JComboBox<String> comboAlojamiento;
    JTable tablaClientes;

    /*VUELO*/
    JTextField txtVueloPuerta;
    JTextField txtVueloAsiento;
    JTextArea txtVueloNumero;
    JComboBox<String> comboBoxVueloDestino;
    JComboBox<String> comboBoxVueloDonde;
    DatePicker dateVueloIda;
    DatePicker dateVueloVuelta;
    JButton btnVueloAnadir;
    JButton btnVueloModificar;
    JButton btnVueloEliminar;
    JButton btnVueloBuscar;
    JTable vueloTabla;
    JTextField txtVueloPrecio;

    /*ALOJAMIENTO*/
    JTextField txtAlojamientoHabitaciones;
    JTextField txtAlojamientoPersonas;
    JComboBox<String> comboBoxAlojamientoDormir;
    JButton btnAlojamientoAnadir;
    JButton btnAlojamientoModificar;
    JButton btnAlojamientoEliminar;
    JButton btnAlojamientoBuscar;
    JTable alojamientoTabla;
    DatePicker entradaAlojamiento;
    DatePicker salidaAlojamiento;
    JComboBox<String> comboBoxVueloClase;
    JTextField txtAlojamientoCodigo;
    JTextField txtAlojamientoParking;
    JTextField txtAlojamientoPrecio;

    /*SEARCH*/
    JLabel etiquetaEstado;

    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmAlojamiento;
    DefaultTableModel dtmVuelo;
    DefaultTableModel dtmCliente;

    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemCrearTabla1;
    JMenuItem itemCrearTabla2;
    JMenuItem itemCrearTabla3;
    JMenuItem itemSalir;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;

    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Vista()  {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);

        //Menus
        setMenu();

        //Combobox y contraseña de administrador
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {
        this.dtmCliente = new DefaultTableModel();
        this.tablaClientes.setModel(dtmCliente);

        this.dtmAlojamiento = new DefaultTableModel();
        this.alojamientoTabla.setModel(dtmAlojamiento);

        this.dtmVuelo = new DefaultTableModel();
        this.vueloTabla.setModel(dtmVuelo);
    }

    /**
     * Setea los con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(Clase constant : Clase.values()) {
            comboBoxVueloClase.addItem(constant.getValor());
        }
        comboBoxVueloClase.setSelectedIndex(-1);

        for(Destinos constant : Destinos.values()) {
            comboBoxVueloDestino.addItem(constant.getValor());
        }
        comboBoxVueloDestino.setSelectedIndex(-1);

        for(Dormir constant : Dormir.values()) {
            comboBoxAlojamientoDormir.addItem(constant.getValor());
        }
        comboBoxAlojamientoDormir.setSelectedIndex(-1);

        for(Destinos constant : Destinos.values()) {
            comboBoxVueloDonde.addItem(constant.getValor());
        }
        comboBoxVueloDonde.setSelectedIndex(-1);
    }

    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la base de datos
     */
    private void setAdminDialog() {
        // Contraseña 1234
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");

        itemDesconectar=new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu crear = new JMenu("Crear");

        itemCrearTabla1 = new JMenuItem("Crear tabla Cliente");
        itemCrearTabla1.setActionCommand("CrearTablaCliente");

        itemCrearTabla2 = new JMenuItem("Crear tabla Vuelo");
        itemCrearTabla2.setActionCommand("CrearTablaVuelo");

        itemCrearTabla3 = new JMenuItem("Crear tabla Alojamiento");
        itemCrearTabla3.setActionCommand("CrearTablaAlojamiento");

        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);

        crear.add(itemCrearTabla1);
        crear.add(itemCrearTabla2);
        crear.add(itemCrearTabla3);
        mbBar.add(crear);

        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);

    }

}
