package com.mariava.agenciaviajes.main.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Creacion de la clase para qu aparezca el panel de la configuracion
 */
public class OptionDialog extends JDialog {
    private JPanel panel1;
    public JButton btnOpcionesGuardar;
    JTextField tfIP;
    JTextField tfUser;
    JPasswordField pfAdmin;
    JPasswordField pfPass;
    private Frame owner;

    /**
     * Construcor de la clase
     * @param owner propietario del dialog
     */
    public OptionDialog(Frame owner) {
        super(owner, "Opciones", true);
        this.owner = owner;
        initDialog();
    }

    /**
     * Inicializo el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(owner);
    }
}


