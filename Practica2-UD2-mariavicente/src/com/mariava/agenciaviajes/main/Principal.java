package com.mariava.agenciaviajes.main;

import com.mariava.agenciaviajes.main.gui.Controlador;
import com.mariava.agenciaviajes.main.gui.Modelo;
import com.mariava.agenciaviajes.main.gui.Vista;

public class Principal {

    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
