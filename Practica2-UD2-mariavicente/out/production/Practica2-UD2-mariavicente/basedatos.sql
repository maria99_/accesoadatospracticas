CREATE DATABASE if not exists agencia2;
--
USE agencia2;
--
create table if not exists vuelo(
    idvuelo int auto_increment primary key,
    numerovuelo int not null,
    clase varchar(20) not null,
    fechaida date,
    fechavuelta date,
    puertaentrada varchar(10) not null,
    asiento varchar(5) not null,
    destino varchar(30) not null,
    donde varchar(30) not null,
    precio float(4));
--
create table if not exists alojamiento(
    idalojamiento int auto_increment primary key,
    codigoalojamiento varchar(30) not null,
    dormir varchar(30) not null,
    numerohabitaciones int not null,
    numeropersonas int not null,
    parking varchar(5),
    entrada date,
    salida date,
    precio float (4));
--
create table if not exists cliente(
    idcliente int auto_increment primary key,
    nombre varchar(50) not null,
    apellidos varchar(50) not null,
    dni varchar(40) not null UNIQUE,
    telefono int not null,
    direccion varchar(50),
    codigopostal varchar(15),
    fechanacimiento date,
    idvuelo int not null,
    idalojamiento int not null);
--
alter table cliente
    add foreign key (idvuelo) references vuelo(idvuelo);
   -- add foreign key (idalojamiento) references alojamiento(idalojamiento);
--
delimiter ||
create function existeDni(f_dni varchar(40))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idcliente) from cliente)) do
            if  ((select dni from cliente where idcliente = (i + 1)) like f_Dni) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNumeroVuelo(f_numero varchar(50))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idvuelo) from vuelo)) do
            if  ((select numerovuelo from vuelo where idvuelo = (i + 1)) like f_numero) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeAlojamiento(f_codigo varchar(202))
    returns bit
begin
    declare i int;
    set i = 0;
    while ( i < (select max(idalojamiento) from alojamiento)) do
            if  ((select codigoalojamiento from alojamiento where idalojamiento = (i + 1)) like f_codigo) then return 1;
            end if;
            set i = i + 1;
        end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create procedure crearTablaCliente()
begin
    create table cliente(
        idcliente int auto_increment primary key,
        nombre varchar(50) not null,
        apellidos varchar(50) not null,
        dni varchar(40) not null UNIQUE,
        telefono int not null,
        direccion varchar(50),
        codigopostal varchar(15),
        fechanacimiento date,
        idvuelo int not null,
        idalojamiento int not null);
end ||
delimiter ;
--
delimiter ||
create procedure crearTablaVuelo()
begin
    create table vuelo(
        idvuelo int auto_increment primary key,
        numerovuelo int not null,
        clase varchar(20) not null,
        fechaida date,
        fechavuelta date,
        puertaentrada varchar(10) not null,
        asiento varchar(5) not null,
        destino varchar(30) not null,
        donde varchar(30) not null,
        precio float(4));
end ||
delimiter ;
--
delimiter ||
create procedure crearTablaAlojamiento()
begin
    create table alojamiento(
        idalojamiento int auto_increment primary key,
        codigoalojamiento varchar(30) not null,
        dormir varchar(30) not null,
        numerohabitaciones int not null,
        numeropersonas int not null,
        parking varchar(5),
        entrada date,
        salida date,
        precio float (4));
end ||
delimiter ;