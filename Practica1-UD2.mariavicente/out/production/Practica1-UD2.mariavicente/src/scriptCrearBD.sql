CREATE DATABASE agencia;

use agencia;


CREATE TABLE cliente(
    id int primary key auto_increment,
    dni varchar(9) unique,
    nombre varchar (40),
    apellido varchar (40),
    direccion varchar(40),
    telefono int,
    destino varchar(40),
    fecha_viaje timestamp
);


INSERT INTO cliente(dni,nombre, apellido, direccion, telefono, destino, fecha_viaje)

VALUES('73629485M', 'Antonio', 'Martin', 'Plaza de los sitios', '638285636', '' ,'2017-05-06');


select * from cliente;



DELIMITER //
CREATE PROCEDURE crearTablaCliente()
BEGIN
    CREATE TABLE cliente(
                            iid int primary key auto_increment,
                            dni varchar(9) unique,
                            nombre varchar (40),
                            apellido varchar (40),
                            direccion varchar(40),
                            telefono int,
                            destino varchar(40),
                            fecha_viaje timestamp);
END //




DELIMITER //
CREATE PROCEDURE mostrarClientesDni()
BEGIN
    SELECT dni,COUNT(*) FROM cliente GROUP BY dni;
END //

