package com.mariava.AgenciaBBDD.main;

import com.mariava.AgenciaBBDD.gui.Controlador;
import com.mariava.AgenciaBBDD.gui.Modelo;
import com.mariava.AgenciaBBDD.gui.Vista;

/**
 * Clase donde se crea la clase vista y la clase modelo con el controlador
 */
public class Principal {
    /**
     * Metodo para inicializar vista y modelo, con nuestro controlador
     * @param args
     */
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
