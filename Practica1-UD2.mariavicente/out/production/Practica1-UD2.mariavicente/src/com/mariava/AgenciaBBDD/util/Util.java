package com.mariava.AgenciaBBDD.util;

import javax.swing.*;

/**
 * Esta es una clase en la que he puesto métodos estáticos para crear una ventana con un mensaje.
 */

public class Util {
    /**
     * Este método muestra un mensaje de error con el texto recibido
     * @param message Texto del mensaje de error
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     *  Este método me muestra un mensaje de información con el texto recibido
     * @param message Texto del mensaje de informacion
     */
    public static void showInfoAlert(String message){
        JOptionPane.showMessageDialog(null, message, "Informacion", JOptionPane.INFORMATION_MESSAGE);
    }

}
