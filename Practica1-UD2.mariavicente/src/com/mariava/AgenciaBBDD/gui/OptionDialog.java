package com.mariava.AgenciaBBDD.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase para crear el menu de opciones para poder conectar con la base de datos
 */
public class OptionDialog extends JDialog {
    /**
     * Creacion de los atributos
     */
    private JPanel panel1;
    public JButton btnOpcionesGuardar;
    JTextField tflP;
    JTextField tfUser;
    JPasswordField pfAdmin;
    JPasswordField pfPass;
    private Frame owner;

    /**
     * Crear el constructor de la clase
     * @param owner propietario
     */
    public OptionDialog(Frame owner) {
        super(owner, "Opciones", true);
        this.owner = owner;
        initDialog();
    }

    /**
     * Inicializamos el JDialog, para darle un tamaño a la ventana y que aparezcan las opciones elegidas
     * anteriormente
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(owner);
    }
}
