package com.mariava.AgenciaBBDD.gui;

public enum Destino {
    MADRID("Madrid"),
    ZARAGOZA("Zaragoza"),
    TERUEL("Teruel"),
    HUESCA("Huesca"),
    BUDAPEST("Budapest"),
    POLONIA("Cracovia"),
    PAISESBAJOS("Amsterdam"),
    GRECIA("Santorini"),
    EXTREMADURA("Caceres"),
    FRANCIA("Paris");

    private String valor;

    Destino(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}
