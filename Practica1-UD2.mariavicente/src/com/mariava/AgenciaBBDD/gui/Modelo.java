package com.mariava.AgenciaBBDD.gui;

import com.mariava.AgenciaBBDD.util.Util;

import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Creacion de la clase modelo, donde principalmente hacemos la conexcion con nuestros metodos
 */
public class Modelo {
    /**
     * Creacion de los atributos a utilizar
     */
    private Connection conexion;
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Creamos el constructor de esta clase, e inicializamos los ArryaList
     * Carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }

    String getUser() {
        return user;
    }

    String getPassword() {
        return password;
    }

    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Creamos una tabla para el cliente, por si anteriormente no la teniamos creada y
     * necesitamos crear una nueva
     *
     * @throws SQLException
     */
    public void crearTablaCliente() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/agencia", "root", "");

        String sentenciaSql = "call crearTablaCliente()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }


    /**
     * Metodo para conectar con nuestra base de datos
     */
    void conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/agencia", user, password);
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/", user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    /**
     * Lee el fichero donde hemos metido todas las funciones y los campos de la base de datos
     * que vamos a utilizar
     *
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        String linea;
        StringBuilder stringBuilder = new StringBuilder();

        BufferedReader reader = new BufferedReader(new FileReader("scriptCrearBD.sql"));
        while ((linea = reader.readLine()) != null) {
            stringBuilder.append(linea);
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }

    /**
     * Metodo para desconectar la base de datos y que no se quede abierta
     */
    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo para tener los datos de nuestra tabla creada anteriormente (clientes) y ver los campos
     *
     * @return resultado
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM cliente";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para que nos cuente cuantas personas tienen ese numero de DNI
     *
     * @return resultado(para saber que es lo que tiene que poner en la base de datos)
     * @throws SQLException
     */
    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "SELECT dni,COUNT(*) FROM cliente GROUP BY dni";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para que una vez que abres la ventana, nos aparecen los campos que tenemos que rellenar.
     * En este caso para insertar un dato a la baase de datos, tenemos que rellenar los campos que
     * aparecen a continucacion y una vez añadimos apareceran en nuestra Jtable y en nuestra base de datos.
     *
     * @param dni        numero de dni de la persona de bbdd
     * @param nombre     nombre de la persona
     * @param apellido   apellido de la persona
     * @param direccion  direccion de la persona
     * @param telefono   telefono de la persona
     * @param destino    destino al que quiere viajar la persona
     * @param fechaViaje fecha en la que quiere ir
     * @return resultado de todo lo anterior
     * @throws SQLException
     */
    public int insertarCliente(String dni, String nombre, String apellido, String direccion, String telefono, String destino, LocalDateTime fechaViaje) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "INSERT INTO cliente(dni, nombre, apellido, direccion, telefono, destino, fecha_viaje)" +
                "VALUES (?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, dni);
        sentencia.setString(2, nombre);
        sentencia.setString(3, apellido);
        sentencia.setString(4, direccion);
        sentencia.setString(5, String.valueOf(telefono));
        sentencia.setString(6, destino);
        sentencia.setTimestamp(7, Timestamp.valueOf(fechaViaje));

        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return numeroRegistros;
    }

    /**
     * Eliminar un cliente que no queramos o no neccesitamos
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public int eliminarCliente(int id) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "DELETE FROM cliente WHERE id=?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return resultado;
    }

    /**
     * Cambiar los datos del cliente.
     * Lo mismo qye al insertar, modificaremos algunos campos y aparecerai en nuestra base de datos cambiado.
     *
     * @param id        numero de identificacion de bbdd
     * @param dni       numero de dni de la persona de bbdd
     * @param nombre    nombre de la persona
     * @param apellido  apellido de la persona
     * @param direccion direccion de la persona
     * @param telefono  telefono de la persona
     * @param destino   destino al que quiere viajar la persona
     * @param fecha     fecha en la que quiere ir
     * @return resultado de todo lo anterior
     * @throws SQLException
     */
    public int modificarCliente(int id, String dni, String nombre, String apellido, String direccion, int telefono, String destino, Timestamp fecha) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "UPDATE cliente SET dni=?, nombre=?," +
                "apellido=?, direccion=?, telefono=?, destino=?, fecha_viaje=? WHERE id=?";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, dni);
        sentencia.setString(2, nombre);
        sentencia.setString(3, apellido);
        sentencia.setString(4, direccion);
        sentencia.setString(5, String.valueOf(telefono));
        sentencia.setString(6, destino);
        sentencia.setTimestamp(7, fecha);
        sentencia.setInt(8, id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return resultado;
    }

    /**
     * Muestra los clientes con ese DNI e indica cuantas personas lo tienen con su id (identificacion)
     *
     * @throws SQLException
     */
    public void clientesPorDni() throws SQLException {
        String sentenciaSql = "call mostrarClientesDni()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }


    /**
     * Metodo para poder buscar los clientes que tengan ese dni
     * @param dni variable llamada dni
     * @return devuleve la variable llamada buscardni, que es la utilizada en todo el metodo
     * @throws SQLException excepcion por si hay error
     */
    public String buscarDni(String dni) throws SQLException {
        PreparedStatement sentencia = null;
        String buscardni = null;

        String consulta = "SELECT dni FROM cliente" + " WHERE dni='" +dni+"'";
        sentencia = conexion.prepareStatement(consulta);

        ResultSet resultado = null;
        try{
            resultado = sentencia.executeQuery(consulta);
            Util.showInfoAlert("Persona con dni existente");
        }catch (Exception e){
            Util.showInfoAlert("No existe persona con ese dni");
        }

        buscardni = asignar();
    return buscardni;
    }

    /**
     * Metotodo que se llama asignar, para que una vez que busques el dni del cliente
     * lo puedas asignar a su tabla correspondiente
     * @return
     */
    public String asignar() {
        PreparedStatement sentencia = null;
        String buscardni = null;
        String dni;
        ResultSet resultado = null;
        try {
            resultado = sentencia.executeQuery();
            if(resultado.first()){
                dni = resultado.getString("dni");
                buscardni = new String(dni);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return buscardni;
    }


    /**
     * Lee el archivo de propiedades y setea los atributos que necesita
     */
    private void getPropValues() {
        Properties config = new Properties();
        InputStream configInput = null;

        try {
            configInput = new FileInputStream("config.properties");

            config.load(configInput);

            ip = config.getProperty("ip");
            user = config.getProperty("user");
            password = config.getProperty("pass");
            adminPassword = config.getProperty("admin");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (configInput != null) configInput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas anteriormente por parametros de nuestro archivo de propiedades
     * @param ip ip de la base de datos
     * @param user usuario de la base de datos
     * @param pass contraseña de la base de datos
     * @param adminPass contraseña del administrador de la base de datos
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties config = new Properties();
            config.setProperty("ip", ip);
            config.setProperty("user", user);
            config.setProperty("pass", pass);
            config.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            config.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

}
