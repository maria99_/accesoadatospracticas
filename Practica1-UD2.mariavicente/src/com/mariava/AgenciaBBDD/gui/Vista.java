package com.mariava.AgenciaBBDD.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase donde se importa y se crea todos los botones o textos donde vas a escribir
 */
public class Vista extends JFrame {
    /**
     * Generar los atributos
     */
    private JPanel panel1;
    JTextField txtDni;
    JTextField txtNombre;
    JTextField txtApellido;
    JTextField txtDireccion;
    JTextField txtTelefono;
    JTextField txtBuscar;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JList list1;
    JButton btnClientesPorDni;
    JTable tabla;
    JLabel lblAccion;
    DateTimePicker dateTimePicker;
    JComboBox comboDestino;
    private JTable tablaDNI;

    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemCrearTabla;
    JMenuItem itemSalir;
    JFrame frame;

    /**
     * Creacion de un constructor, con difererentes metodos y para que sea visible a ventana
     */
    public Vista(){
        frame = new JFrame("Agencia de Viaje");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(800, 200, 2000, 600);
        dtm =new DefaultTableModel();
        tabla.setModel(dtm);

        dtm1 =new DefaultTableModel();
        tablaDNI.setModel(dtm1);

        crearMenu();
        optionDialog = new OptionDialog(this);
        setAdminDialog();

        setEnumComboBox();

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Creacion de un metodo que te devuelve unos valores sobre un menu
     */
    private void crearMenu() {
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar=new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemCrearTabla = new JMenuItem("Crear tabla Cliente");
        itemCrearTabla.setActionCommand("CrearTablaCliente");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemOpciones);
        menuArchivo.add(itemDesconectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);

    }


    private void setEnumComboBox() {
        for(Destino constant: Destino.values()) {
            comboDestino.addItem(constant.getValor());
        }
        comboDestino.setSelectedIndex(-1);
    }


    /**
     * Metodo para validad el administrador (para conectar a la base de datos) y que se te
     * abra las opciones, que en este caso introduces primero la contraseña y luego veras la ip, usuario...
     */
    private void setAdminDialog() {
        //contraseña para validar 1234
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
