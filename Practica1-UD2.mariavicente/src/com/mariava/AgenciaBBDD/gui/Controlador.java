package com.mariava.AgenciaBBDD.gui;

import com.mariava.AgenciaBBDD.util.Util;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Creacion de la clase controlador con nuestro metodos de vista y de modelo
 */
public class Controlador implements ActionListener, TableModelListener {

    /**
     * Creacion de los atributos a utilizar
     */
    private Vista vista;
    private Modelo modelo;

    /**
     * Creacion del constructor
     * @param vista donde se escribe toda la informacion
     * @param modelo metodos que se crean para conectar con la bbdd
     */
    public Controlador(Vista vista, Modelo modelo){
        this.modelo = modelo;
        this.vista = vista;

        setOptions();
        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);
    }

    /**
     * Dar nombres a las columnas de la tabla
     */
    private void iniciarTabla() {
        String[] headers={"id","DNI","Nombre","Apellido","Direccion", "Telefono", "Destino", "Fecha"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Dar nombres a las columnas de la tabla
     */
    private void iniciarTabla1() {
        String[] headers={"Dni","Cuantas personas tienen el dni"};
        vista.dtm1.setColumnIdentifiers(headers);
    }

    private void addTableModelListeners(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
    }

    /**
     * Creacion de los listener para que funcionen los botones
     * @param listener variable llamada asi para incilializar
     */
    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnClientesPorDni.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tflP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Modificar las tablas en el caso que se necesitara
     * @param e llamado asi la variable para ir actualizando durante la ejecucion
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("Actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarCliente((Integer) vista.dtm.getValueAt(filaModicada,0),
                        (String)vista.dtm.getValueAt(filaModicada,1),
                        (String)vista.dtm.getValueAt(filaModicada,2),
                        (String)vista.dtm.getValueAt(filaModicada,3),
                        (String)vista.dtm.getValueAt(filaModicada,4),
                        (Integer)vista.dtm.getValueAt(filaModicada,5),
                        (String)vista.dtm.getValueAt(filaModicada,6),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModicada,7));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * Diferentes opciones en un switch para elegir dependiendo de la copcion que cojas una opcion
     * u otra
     * @param e llamado asi la variable para ir actualizando durante la ejecucion
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;

            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                   modelo.conectar();
                   vista.lblAccion.setText("Conectado");
                   vista.adminPassword.setText("");
                   vista.adminPasswordDialog.dispose();
                   vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;

            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tflP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Vista(), new Modelo());
                break;

            case "desconectar":
                modelo.desconectar();
                vista.lblAccion.setText("Desconectado");
                break;

            case "Clientes por DNI":
                try{
                    modelo.clientesPorDni();
                    cargarFilas1(modelo.obtenerDatos1());
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "crearTablaCliente":
                try{
                    modelo.crearTablaCliente();
                    vista.lblAccion.setText("Tabla Clientes creada");
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "Nuevo":
                try{
                    modelo.insertarCliente(vista.txtDni.getText(),
                            vista.txtNombre.getText(),
                            vista.txtApellido.getText(),
                            vista.txtDireccion.getText(),
                            vista.txtTelefono.getText(),
                            (String) vista.comboDestino.getSelectedItem(),
                            vista.dateTimePicker.getDateTimePermissive());
                    cargarFilas(modelo.obtenerDatos());
                }catch (SQLException e1){
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                String dni = vista.btnBuscar.getText();
                String buscardni = null;
                try {
                    buscardni = modelo.buscarDni(dni);
                    if (buscardni == null){

                    }else{
                        mostrar(buscardni);
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                break;

            case "Eliminar":
                try {
                    int filaBorrar=vista.tabla.getSelectedRow();
                    int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarCliente(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

        }
    }

    /**
     * Metodo para que busque los clientes con ese DNI
     * @param buscardni nombre e la variable para buscar los datos
     */
    private void mostrar(String buscardni) {
        vista.txtBuscar.setText(buscardni);
    }


    /**
     * Metodo para que todos los datos escritos aparezcan en la tabla y podamos verlos
     * @param resultSet llamado asi la variable para ir actualizando durante la ejecucion
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);
            fila[7]=resultSet.getObject(8);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }
    }

    /**
     * Metodo para que todos los datos escritos aparezcan en la tabla(dni) y podamos verlos saber cuantos dnis hay.
     * @param resultSet llamado asi la variable para ir actualizando durante la ejecucion
     * @throws SQLException
     */
    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

}
