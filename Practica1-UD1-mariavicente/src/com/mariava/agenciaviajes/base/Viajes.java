package com.mariava.agenciaviajes.base;

/**
 * Clase Viajes
 */
public class Viajes {
    String destino;
    String alojamiento;
    String nombreCliente;
    String comentario;

    /**
     * Creacion del constructor
     * @param destino
     * @param alojamiento
     * @param nombreCliente
     * @param comentario
     */
    public Viajes(String destino, String alojamiento, String nombreCliente, String comentario) {
        this.destino = destino;
        this.alojamiento = alojamiento;
        this.nombreCliente = nombreCliente;
        this.comentario = comentario;
    }

    /**
     * Creacion de getter de destino
     * @return gesDestino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * Creacion de setter de destino
     * @param destino
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * Creacion de getter de alojamiento
     * @return getAlojamiento
     */
    public String getAlojamiento() {
        return alojamiento;
    }

    /**
     * Creacion de setter de alojamiento
     * @param alojamiento
     */
    public void setAlojamiento(String alojamiento) {
        this.alojamiento = alojamiento;
    }

    /**
     * Creacion de getter de nombre del cliente
     * @return getNombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Creacion de setter de nombre de cliente
     * @param nombreCliente
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * Creacion de getter de comentario del cliente
     * @return getComentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Creacion de setter de comentario
     * @param comentario
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    /**
     * Creacion del metodo tostring
     * @return toString
     */
    @Override
    public String toString() {
        return "Viajes{" +
                "destino='" + destino + '\'' +
                ", alojamiento='" + alojamiento + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", comentario='" + comentario + '\'' +
                '}';
    }
}
