import com.mariava.agenciaviajes.base.Viajes;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;


/**
 * Crear clase vista
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JTextField destinoTxt;
    private JTextField alojamientoTxt;
    private JLabel lblDestino;
    private JButton altaDestinoBtn;
    private JButton cancelarBtn;
    private JButton mostrarBtn;
    private JComboBox comboBox;
    private JTextArea comentarioTxt;
    private JTextField nombreTxt;

    private LinkedList<Viajes> lista;
    private DefaultComboBoxModel<Viajes> dtm;

    /**
     * Creacion de metodo vista para configurar botones y ventana
     */
    public Vista() {
        frame = new JFrame("Agencia de Viajes");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBounds(100, 200, 800, 500);
        frame.setVisible(true);

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dtm = new DefaultComboBoxModel<>();
        comboBox.setModel(dtm);

        /**
         * Dar funcionalidad al boton de alta destino
         */
        altaDestinoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaDestino(destinoTxt.getText(), alojamientoTxt.getText(), nombreTxt.getText(), comentarioTxt.getText());
                refrescarCombo();
            }
        });

        /**
         * Dar funcionalidad al boton de mostrar
         */
        mostrarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Viajes seleccionado = (Viajes) dtm.getSelectedItem();
                lblDestino.setText(seleccionado.toString());
            }
        });

        /**
         * Dar funcionalidad al boton de cancelar
         */
        cancelarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                   dtm.removeAllElements();
            }
        });
    }

    /**
     * Creacion de metodo para seleccionar y eliminar el combo box
     */
    private void refrescarCombo() {
        dtm.removeAllElements();
        for (Viajes viaje : lista){
            dtm.addElement(viaje);
        }
    }

    /**
     * Creacion del metodo funcional, con su ventana
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    /**
     * Creacion del metodo pasado con parametros, "constructor"
     * @param destino
     * @param alojamiento
     * @param nombreCliente
     * @param comentario
     */
    private void altaDestino(String destino, String alojamiento, String nombreCliente, String comentario){
        lista.add(new Viajes(destino, alojamiento, nombreCliente, comentario));
    }

    /**
     * Creacion del menu
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXMl = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXMl = new JMenuItem("Importar XML");
        JMenuItem itemSalir = new JMenuItem("Salir");

        itemExportarXMl.addActionListener(new ActionListener() {
            /**
             * Creacion del action listener para crear el metodo performed, y elegir la opcion de exportar fichero
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });

        itemImportarXMl.addActionListener(new ActionListener() {
            /**
             * Creacion del action listener para crear el metodo performed, y elegir la opcion de importar fichero
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showSaveDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarCombo();
                }
            }
        });

        itemSalir.addActionListener(new ActionListener() {
            /**
             * Creacion del action listener para crear el metodo performed, y elegir la opcion de salir del programa
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menu.add(itemExportarXMl);
        menu.add(itemImportarXMl);
        menu.add(itemSalir);

        barra.add(menu);
        frame.setJMenuBar(barra);

    }

    /**
     * Creacion del metodo para importar archivo o fichero xml
     * @param fichero
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            NodeList viajes = documento.getElementsByTagName("Viajes");
            for(int i = 0; i<viajes.getLength(); i++){
                Node viaje = viajes.item(i);
                Element elemento = (Element) viaje;

                String destino = elemento.getElementsByTagName("Destino").item(0).getChildNodes().item(0).getNodeValue();
                String alojamiento = elemento.getElementsByTagName("Alojamiento").item(0).getChildNodes().item(0).getNodeValue();
                String nombreCliente = elemento.getElementsByTagName("NombreCliente").item(0).getChildNodes().item(0).getNodeValue();
                String comentario = elemento.getElementsByTagName("ComentarioCliente").item(0).getChildNodes().item(0).getNodeValue();

                altaDestino(destino, alojamiento, nombreCliente, comentario);
            }

        }catch (ParserConfigurationException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }catch (SAXException e){
            e.printStackTrace();
        }

    }

    /**
     * Creacion del metodo para exportar archivo o fichero xml
     * @param fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try{
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            Document documento = dom.createDocument(null, "xml", null);

            Element raiz = documento.createElement("Viajes");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoViaje;
            Element nodoDatos;
            Text dato;

            for(Viajes viajes : lista){
                nodoViaje = documento.createElement("Viajes");
                raiz.appendChild(nodoViaje);

                nodoDatos = documento.createElement("NombreCliente");
                nodoViaje.appendChild(nodoDatos);

                dato = documento.createTextNode(viajes.getNombreCliente());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Destino");
                nodoViaje.appendChild(nodoDatos);

                dato = documento.createTextNode(viajes.getDestino());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("Alojamiento");
                nodoViaje.appendChild(nodoDatos);

                dato = documento.createTextNode(viajes.getAlojamiento());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("ComentarioCliente");
                nodoViaje.appendChild(nodoDatos);

                dato = documento.createTextNode(viajes.getComentario());
                nodoDatos.appendChild(dato);
            }

            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        }catch (TransformerConfigurationException e){
            e.printStackTrace();
        }catch (TransformerException e){
            e.printStackTrace();
        }catch (ParserConfigurationException e){
            e.printStackTrace();
        }
    }

}
