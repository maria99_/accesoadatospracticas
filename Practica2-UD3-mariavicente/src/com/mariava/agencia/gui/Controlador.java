package com.mariava.agencia.gui;

import com.mariava.agencia.clases.*;
import com.mariava.agencia.vistas.Vista;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase donde implementamos los metodos e inicializamos los botones
 */
public class Controlador implements ActionListener, ListSelectionListener, FocusListener {

    // Atributos
    private Vista vista;
    private Modelo modelo;

    /**
     * Creacion del constructor
     * @param vista declaracion de vista
     * @param modelo declaracion de modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addFocusListeners(this);
        addActionListeners(this);
        addListSelectionListeners(this);
    }

    /**
     * Metodo que inicias los botones de la pestaña vista
     * @param controlador variable llamada asi
     */
    private void addActionListeners(ActionListener controlador) {
        vista.conexionItem.addActionListener(controlador);
        vista.salirItem.addActionListener(controlador);

        vista.btnNuevo.addActionListener(controlador);
        vista.btnEliminar.addActionListener(controlador);
        vista.btnModificar.addActionListener(controlador);

        vista.btnAltaDestino.addActionListener(controlador);
        vista.btnEliminarDestino.addActionListener(controlador);
        vista.btnModificarDestino.addActionListener(controlador);

        vista.btnAltaExcursion.addActionListener(controlador);
        vista.btnEliminarExcursion.addActionListener(controlador);
        vista.btnModificarExcursion.addActionListener(controlador);

        vista.altaButton.addActionListener(controlador);
        vista.eliminarButton.addActionListener(controlador);
        vista.modificarButton.addActionListener(controlador);
    }

    /**
     * Metodo que inicializa las listas de la clase vista
     * @param controlador variable llamada asi
     */
    private void addListSelectionListeners(ListSelectionListener controlador){
        vista.listarClientes.addListSelectionListener(controlador);
        vista.listExcursion.addListSelectionListener(controlador);
        vista.listDestino.addListSelectionListener(controlador);
        vista.listDetalle.addListSelectionListener(controlador);
    }

    /**
     * Metodo para que funcionen los botones y las listas, añadir metodos
     * @param e variable llamada asi
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        if(vista.tabbedPane.getSelectedComponent() == vista.clientePanel) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "Conectar":
                    vista.conexionItem.setEnabled(false);
                    modelo.conectar();
                    vista.txtEstado.setText("Estado: Conectado");
                    break;

                case "AltaCliente":
                    Cliente nuevoCliente = new Cliente();
                    nuevoCliente.setDni(vista.txtDni.getText());
                    nuevoCliente.setEmail(vista.txtEmail.getText());
                    nuevoCliente.setNombre(vista.txtNombre.getText());
                    nuevoCliente.setDireccion(vista.txtDireccion.getText());
                    nuevoCliente.setTelefono(vista.txtTelefono.getText());
                    modelo.altaCliente(nuevoCliente);
                    vista.txtEstado.setText("Estado: Cliente añadido");
                    break;

                case "ModificarCliente":
                    Cliente clienteSeleccion = (Cliente) vista.listarClientes.getSelectedValue();
                    clienteSeleccion.setDni(vista.txtDni.getText());
                    clienteSeleccion.setEmail(vista.txtEmail.getText());
                    clienteSeleccion.setNombre(vista.txtNombre.getText());
                    clienteSeleccion.setDireccion(vista.txtDireccion.getText());
                    clienteSeleccion.setTelefono(vista.txtTelefono.getText());
                    modelo.modificarCliente(clienteSeleccion);
                    vista.txtEstado.setText("Estado: Cliente Modificado");
                    break;

                case "BorrarCliente":
                    Cliente clienteBorrado = (Cliente)vista.listarClientes.getSelectedValue();
                    modelo.borrarCliente(clienteBorrado);
                    vista.txtEstado.setText("Estado: Cliente Borrado");
                    break;

            }
        } else if(vista.tabbedPane.getSelectedComponent() == vista.excursionPanel) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "NuevaExcursion":
                    Excursion nuevaExcursion = new Excursion();
                    nuevaExcursion.setCodigo(vista.txtCodigo.getText());
                    nuevaExcursion.setNombreExcursion(vista.txtNombreExcursion.getText());
                    nuevaExcursion.setDescripcion(vista.txtDescripcion.getText());
                    nuevaExcursion.setPrecio(Double.valueOf(vista.txtPrecio.getText()));
                    modelo.altaExcursion(nuevaExcursion);

                    vista.txtEstado.setText("Estado: Excursion añadido");
                    break;
                case "ModificarExcursion":
                    Excursion excursionSeleccion = new Excursion();
                    excursionSeleccion.setCodigo(vista.txtCodigo.getText());
                    excursionSeleccion.setNombreExcursion(vista.txtNombreExcursion.getText());
                    excursionSeleccion.setDescripcion(vista.txtDescripcion.getText());
                    excursionSeleccion.setPrecio(Double.valueOf(vista.txtPrecio.getText()));
                    modelo.modificarExcursion(excursionSeleccion);

                    vista.txtEstado.setText("Estado: Excursion modificada");
                    break;

                case "BorrarExcursion":
                    Excursion excursionBorrar = (Excursion) vista.listExcursion.getSelectedValue();
                    modelo.borrarExcursion(excursionBorrar);

                    vista.txtEstado.setText("Estado: Excursion borrada");
                    break;
            }
        } else if(vista.tabbedPane.getSelectedComponent() == vista.destinoPanel) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "NuevoDestino":
                    Destino nuevoDestino = new Destino();
                    nuevoDestino.setNombreDestino(vista.txtnombreDestino.getText());
                    nuevoDestino.setDescripcion(vista.txtDescripcionDestino.getText());
                    nuevoDestino.setFecha(Date.valueOf(vista.fechaDestino.getDate()));

                    modelo.altaDestino(nuevoDestino);

                    vista.txtEstado.setText("Estado: Destino añadido");
                    break;
                case "ModificarDestino":
                    Destino destinoSeleccion = (Destino) vista.listDestino.getSelectedValue();
                    destinoSeleccion.setNombreDestino(vista.txtnombreDestino.getText());
                    destinoSeleccion.setDescripcion(vista.txtDescripcionDestino.getText());
                    destinoSeleccion.setFecha(Date.valueOf(vista.fechaDestino.getDate()));
                    modelo.modificarDestino(destinoSeleccion);

                    vista.txtEstado.setText("Estado: Destino modificado");
                    break;
                case "BorrarDestino":
                    Destino destinoBorrar = (Destino) vista.listDestino.getSelectedValue();
                    modelo.borrarDestino(destinoBorrar);

                    vista.txtEstado.setText("Estado: Destino borrado");
                    break;
            }
        } else if(vista.tabbedPane.getSelectedComponent() == vista.detallePanel) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "NuevoDetalle":
                    DetalleGuia nuevoDetalle = new DetalleGuia();
                    nuevoDetalle.setCantidad(Integer.parseInt(vista.txtCantidadDetalle.getText()));
                    nuevoDetalle.setPrecio(Double.valueOf(vista.txtPrecioDetalle.getText()));
                    modelo.altaDetalleGuia(nuevoDetalle);

                    vista.txtEstado.setText("Estado: Detalle añadido");
                    break;

                case "ModificarDetalle":
                    DetalleGuia modificarDetalle = (DetalleGuia) vista.listDetalle.getSelectedValue();
                    modificarDetalle.setCantidad(Integer.parseInt(vista.txtCantidadDetalle.getText()));
                    modificarDetalle.setPrecio(Double.valueOf(vista.txtPrecioDetalle.getText()));
                    modelo.modificarDetalle(modificarDetalle);

                    vista.txtEstado.setText("Estado: Detalle modificado");
                    break;

                case "BorrarDetalle":
                    DetalleGuia detalleBorrado = (DetalleGuia) vista.listDetalle.getSelectedValue();
                    modelo.borrarDetalle(detalleBorrado);

                    vista.txtEstado.setText("Estado: Detalle Borrado");
                    break;
            }
        }
        listarClientes(modelo.getCliente());
        listarExcursion(modelo.getExcursions());
        listarDestino(modelo.getDestinos());
        listarDetalle(modelo.getDetalleGuias());
    }

    /**
     * Metodo que lista los guias
     * @param lista variable llamada asi
     */
    private void listarDetalle(ArrayList<DetalleGuia> lista) {
        vista.dlmDetalle.clear();
        for(DetalleGuia unDetalle: lista){
            vista.dlmDetalle.addElement(unDetalle);
        }
    }

    /**
     * Metodo que lista el destino
     * @param lista variable llamada asi
     */
    private void listarDestino(ArrayList<Destino> lista) {
        vista.dlmDestino.clear();
        for(Destino unDestino: lista){
            vista.dlmDestino.addElement(unDestino);
        }
    }

    /**
     * Metodo que lista la excursion
     * @param lista variable llamada asi
     */
    private void listarExcursion(ArrayList<Excursion> lista) {
        vista.dlmExcursion.clear();
        for(Excursion unaExcursion: lista){
            vista.dlmExcursion.addElement(unaExcursion);
        }
    }

    /**
     * Metodo que lista los clientes
     * @param lista variable llamada asi
     */
    private void listarClientes(ArrayList<Cliente> lista) {
        vista.dlmClientes.clear();
        for(Cliente unCliente: lista){
            vista.dlmClientes.addElement(unCliente);
        }
    }

    /**
     * Metodo para añadir lo que escribes en la clase vista
     * @param e variable llamada asi
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listarClientes) {
                Cliente clienteSeleccion = (Cliente) vista.listarClientes.getSelectedValue();
                vista.txtDni.setText(clienteSeleccion.getDni());
                vista.txtEmail.setText(clienteSeleccion.getEmail());
                vista.txtNombre.setText(clienteSeleccion.getNombre());
                vista.txtDireccion.setText(clienteSeleccion.getDireccion());
                vista.txtTelefono.setText(clienteSeleccion.getTelefono());
                vista.comboDestino.setSelectedItem(clienteSeleccion.getContratas());
            }
            if(e.getSource() == vista.listExcursion) {
                Excursion excursionSeleccion = (Excursion) vista.listExcursion.getSelectedValue();
                vista.txtCodigo.setText(excursionSeleccion.getCodigo());
                vista.txtNombreExcursion.setText(excursionSeleccion.getNombreExcursion());
                vista.txtDescripcion.setText(excursionSeleccion.getDescripcion());
                vista.txtPrecio.setText(String.valueOf(excursionSeleccion.getPrecio()));
            }
            if(e.getSource() == vista.listDestino) {
                Destino destinoSeleccion = (Destino) vista.listDestino.getSelectedValue();
                vista.txtnombreDestino.setText(destinoSeleccion.getNombreDestino());
                vista.txtDescripcionDestino.setText(destinoSeleccion.getDescripcion());
                vista.fechaDestino.setText(String.valueOf(destinoSeleccion.getFecha()));
                vista.comboExcursion.setSelectedItem(destinoSeleccion.getExcursiones());
            }
            if(e.getSource() == vista.listGuia) {
                Guia guiaSeleccion = (Guia) vista.listGuia.getSelectedValue();
                vista.txtNombreGuia.setText(guiaSeleccion.getNombreGuia());
                vista.fechaGuia.setText(String.valueOf(guiaSeleccion.getFechaContratacion()));
                vista.txtPrecioGuia.setText(String.valueOf(guiaSeleccion.getPrecioTotal()));
            }
        }
    }

    /**
     * Añadir los listeners en los combobox de la seccion destino y excursion
     * @param controlador variable llamada asi
     */
    private void addFocusListeners(FocusListener controlador){
        vista.comboDestino.addFocusListener(controlador);
        vista.comboExcursion.addFocusListener(controlador);
    }

    /**
     * Cuando seleccionas un combo se selecciona
     * @param e
     */
    @Override
    public void focusGained(FocusEvent e) {
        if(e.getSource() == vista.comboExcursion) {
            vista.comboExcursion.hidePopup();
            listarExcursionComboBox();
            vista.comboExcursion.showPopup();
        }
        if(e.getSource() == vista.comboDestino) {
            vista.comboDestino.hidePopup();
            listarDestinoComboBox();
            vista.comboDestino.showPopup();
        }
    }

    /**
     * Listar el combo box de destino
     */
    private void listarDestinoComboBox() {
        List<Destino> listaDestino = modelo.getDestinos();
        vista.dcbmDestino.removeAllElements();
        for(Destino destino : listaDestino){
            vista.dcbmDestino.addElement(destino);
        }
    }

    /**
     * Listar el combo box de excursiones
     */
    private void listarExcursionComboBox() {
        List<Excursion> listaExcursion = modelo.getExcursions();
        vista.dcbmExcursion.removeAllElements();
        for(Excursion excursion : listaExcursion){
            vista.dcbmExcursion.addElement(excursion);
        }
    }

    // Metodos no necesarios de utilizar, pero si necesario de implementar
    @Override
    public void focusLost(FocusEvent e) {

    }
}

