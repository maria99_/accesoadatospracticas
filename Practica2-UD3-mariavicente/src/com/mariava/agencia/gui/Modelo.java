package com.mariava.agencia.gui;

import com.mariava.agencia.clases.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase para realizar todas las operaciones con Hibernate
 */
public class Modelo {

    // Atributo de sesion
    SessionFactory sessionFactory;

    /**
     * Metodo que desconecta la sesion de hibernnate
     */
    public void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo que conecta con la sesion de hibernate
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Destino.class);
        configuracion.addAnnotatedClass(Excursion.class);
        configuracion.addAnnotatedClass(Guia.class);
        configuracion.addAnnotatedClass(ContrataVisita.class);
        configuracion.addAnnotatedClass(DestinoExcursion.class);
        configuracion.addAnnotatedClass(DetalleGuia.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Dar de alta un cliente con hibernate
     * @return lista, variable llamada asi
     */
    public ArrayList<Cliente> getCliente() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Dar de alta un destino con hibernate
     * @return lista, variable llamada asi
     */
    public ArrayList<Destino> getDestinos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Destino ");
        ArrayList<Destino> lista2 = (ArrayList<Destino>)query.getResultList();
        sesion.close();
        return lista2;
    }

    /**
     * Dar de alta una excursion con hibernate
     * @return lista, variable llamada asi
     */
    public ArrayList<Excursion> getExcursions() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Excursion ");
        ArrayList<Excursion> lista3 = (ArrayList<Excursion>)query.getResultList();
        sesion.close();
        return lista3;
    }

    /**
     * Dar de alta un detalle de guia con hibernate
     * @return lista, variable llamada asi
     */
    public ArrayList<DetalleGuia> getDetalleGuias() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM DetalleGuia ");
        ArrayList<DetalleGuia> lista6 = (ArrayList<DetalleGuia>)query.getResultList();
        sesion.close();
        return lista6;
    }

    /**
     * Dar de alta a un cliente
     * @param nuevoCliente variable llamada asi
     */
    public void altaCliente(Cliente nuevoCliente) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Dar de alta un destino
     * @param nuevoDestino variable llamada asi
     */
    public void altaDestino(Destino nuevoDestino) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoDestino);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Dar de alta a un nuevo guia
     * @param nuevoDetalle variable llamada asi
     */
    public void altaDetalleGuia(DetalleGuia nuevoDetalle) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoDetalle);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Dar de alta una excursion
     * @param nuevaExcursion variable llamada asi
     */
    public void altaExcursion(Excursion nuevaExcursion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaExcursion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Mosdificar un cliente
     * @param clienteSeleccionado variable llamada asi
     */
    public void modificarCliente(Cliente clienteSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Modioficar un destino
     * @param destinoSeleccionado variable llamada asi
     */
    public void modificarDestino(Destino destinoSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(destinoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Modificar un guia
     * @param detalleSeleccionado variable llamada asi
     */
    public void modificarDetalle(DetalleGuia detalleSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(detalleSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Modificar una excursion
     * @param excursionSeleccionado variable llamada asi
     */
    public void modificarExcursion(Excursion excursionSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(excursionSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Borrar un cliente
     * @param cliente variable llamada asi
     */
    public void borrarCliente(Cliente cliente){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cliente);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Borrar un destino
     * @param destinoBorrado variable llamada asi
     */
    public void borrarDestino(Destino destinoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(destinoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Borrar un guia
     * @param detalleBorrar variable llamada asi
     */
    public void borrarDetalle(DetalleGuia detalleBorrar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(detalleBorrar);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Borrar una excursion
     * @param excursionBorrar variable llamada asi
     */
    public void borrarExcursion(Excursion excursionBorrar) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(excursionBorrar);
        sesion.getTransaction().commit();
        sesion.close();
    }


}
