package com.mariava.agencia.vistas;

import com.github.lgooddatepicker.components.DatePicker;
import com.mariava.agencia.clases.Destino;
import com.mariava.agencia.clases.Excursion;

import javax.swing.*;

/**
 * Clase donde damos de alta todos los botones y todo lo necesario para usar en la ventana
 */
public class Vista extends JFrame{
    private JPanel panel1;
    private JFrame frame;
    public JTabbedPane tabbedPane;

    // Atriutos Cliente
    public JPanel clientePanel;
    public JTextField txtId;
    public JTextField txtNombre;
    public JTextField txtDni;
    public JTextField txtEmail;
    public JTextField txtDireccion;
    public JTextField txtTelefono;
    public JButton btnNuevo;
    public JButton btnEliminar;
    public JButton btnModificar;


    // Atributos Destino
    public JPanel destinoPanel;
    public JTextField txtIdDestino;
    public JTextField txtnombreDestino;
    public JTextField txtDescripcionDestino;
    public DatePicker fechaDestino;
    public JButton btnAltaDestino;
    public JButton btnEliminarDestino;
    public JButton btnModificarDestino;
    public JList listDestino;

    // Atributos Excursion
    public JPanel excursionPanel;
    public JTextField idExcursion;
    public JTextField txtNombreExcursion;
    public JTextField txtDescripcion;
    public JTextField txtPrecio;
    public JTextField txtCodigo;
    public JButton btnAltaExcursion;
    public JButton btnEliminarExcursion;
    public JButton btnModificarExcursion;
    public JList listExcursion;

    public JTextField txtNombreGuia;
    public JTextField txtPrecioGuia;
    public DatePicker fechaGuia;
    public JButton btnAltaGuia;
    public JButton btnEliminarGuia;
    public JButton btnModificarGuia;
    public JList listGuia;

    // Etiqueta estado
    public JLabel txtEstado;
    public JList listarClientes;
    public JComboBox<Destino> comboGuia;
    public JComboBox<Destino> comboDestino;
    public JComboBox<Excursion> comboExcursion;

    // Atributos detalle
    public JTextField txtIdDetalle;
    public JTextField txtCantidadDetalle;
    public JTextField txtPrecioDetalle;
    public JButton eliminarButton;
    public JButton modificarButton;
    public JList listDetalle;
    public JPanel detallePanel;
    public JButton altaButton;


    // DefaultListModel
    public DefaultListModel dlmClientes;
    public DefaultListModel dlmGuia;
    public DefaultListModel dlmExcursion;
    public DefaultListModel dlmDestino;
    public DefaultListModel dlmDetalle;

    // DefaultComboBox
    public DefaultComboBoxModel<Destino> dcbmCliente;
    public DefaultComboBoxModel<Destino> dcbmDestino;
    public DefaultComboBoxModel<Excursion> dcbmExcursion;

    // JMenu Item
    public JMenuItem conexionItem;
    public JMenuItem salirItem;

    /**
     * Constructor con sus metodos
     */
    public Vista() {
        frame = new JFrame("Agencia Mapeo");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
       // frame.pack();
        frame.setBounds(100, 300, 800, 640);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        crearMenu();
        inicializarCliente();
        iniciarComboBox();
        inicializarDestino();
        inicializarExcursion();
        inicializarDetalle();
    }


    /**
     *  Crear un menu donde declaramos algunas partes de la ventana
     */
    private void crearMenu() {

        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Metodo que inicializa la lista de detalle
     */
    private void inicializarDetalle() {
        dlmDetalle = new DefaultListModel();
        listDetalle.setModel(dlmDetalle);
    }

    /**
     * Metodo que inicializa la lista de cliente
     */
    private void inicializarCliente(){
        dlmClientes = new DefaultListModel<>();
        listarClientes.setModel(dlmClientes);
    }

    /**
     * Metodo que inicializa la lista de destino
     */
    private void inicializarDestino() {
        dlmDestino = new DefaultListModel<>();
        listDestino.setModel(dlmDestino);
    }

    /**
     * Metodo que inicializa la lista de excursion
     */
    private void inicializarExcursion() {
        dlmExcursion = new DefaultListModel<>();
        listExcursion.setModel(dlmExcursion);
    }

    /**
     * Metodo que inicializa los combo box
     */
    private void iniciarComboBox(){
       dcbmCliente = new DefaultComboBoxModel<Destino>();
       comboDestino.setModel(dcbmCliente);

       dcbmExcursion = new DefaultComboBoxModel<>();
       comboExcursion.setModel(dcbmExcursion);

       dcbmDestino = new DefaultComboBoxModel<>();
       comboDestino.setModel(dcbmDestino);

    }

}
