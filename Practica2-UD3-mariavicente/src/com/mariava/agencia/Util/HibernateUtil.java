package com.mariava.agencia.Util;

import com.mariava.agencia.clases.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Clase donde llamamos a hibernate para que haga sus metodos
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static Session session;

    /**
     * Crear la sesion con hibernate y añadir las clases mapeadas
     */
    public static void buildSessionFactory() {
        Configuration configuracion = new Configuration();
        configuracion.configure();

        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Destino.class);
        configuracion.addAnnotatedClass(Excursion.class);
        configuracion.addAnnotatedClass(Guia.class);
        configuracion.addAnnotatedClass(ContrataVisita.class);
        configuracion.addAnnotatedClass(DestinoExcursion.class);
        configuracion.addAnnotatedClass(DetalleGuia.class);


        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();
        sessionFactory = configuracion.buildSessionFactory(serviceRegistry);
    }

    /**
     * Abre una nueva sesion
     */
    public static void openSession() {
        session = sessionFactory.openSession();
    }

    /**
     * Devuelve la sesion actual
     * @return session
     */
    public static Session getCurrentSession() {

        if ((session == null) || (!session.isOpen()))
            openSession();

        return session;
    }

    /**
     * Cierra hibernate
     */
    public static void closeSessionFactory() {

        if (session != null)
            session.close();

        if (sessionFactory != null)
            sessionFactory.close();
    }
}