package com.mariava.agencia.Util;

import javax.swing.*;

/**
 * Clase donde doy de alta lo necesario para que me muestre mensajes de dialogo si hay algun error.
 */
public class Util {

    /**
     * Metodo que me devuelve el error
     * @param mensaje
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }
}
