package com.mariava.agencia.principal;

import com.mariava.agencia.vistas.Vista;
import com.mariava.agencia.gui.Controlador;
import com.mariava.agencia.gui.Modelo;

/**
 * Clase principal donde se va a ejecutar la aplicacion
 */
public class Principal {
    /**
     * Metodo que se utilizar para arrancar la aplicacion
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
