package com.mariava.agencia.clases;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Guia")
public class Guia  {
    private Long id;
    private String nombreGuia;
    private Date fechaContratacion;
    private Double precioTotal;
    private Cliente cliente;
    private List<DetalleGuia> detalles;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_guia")
    public String getNombreGuia() {
        return nombreGuia;
    }

    public void setNombreGuia(String nombreGuia) {
        this.nombreGuia = nombreGuia;
    }

    @Basic
    @Column(name = "fecha_contratacion")
    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Basic
    @Column(name = "precio_total")
    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guia guia = (Guia) o;
        return Objects.equals(id, guia.id) && Objects.equals(nombreGuia, guia.nombreGuia) && Objects.equals(fechaContratacion, guia.fechaContratacion) && Objects.equals(precioTotal, guia.precioTotal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreGuia, fechaContratacion, precioTotal);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @OneToMany(mappedBy = "guia")
    public List<DetalleGuia> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleGuia> detalles) {
        this.detalles = detalles;
    }


    @Override
    public String toString() {
        return nombreGuia;
    }
}
