package com.mariava.agencia.clases;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "guia_excursion", schema = "agenciamaria", catalog = "")
public class DetalleGuia {
    private Long id;
    private int cantidad;
    private Double precio;
    private Guia guia;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetalleGuia that = (DetalleGuia) o;
        return cantidad == that.cantidad && Objects.equals(id, that.id) && Objects.equals(precio, that.precio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad, precio);
    }

    @ManyToOne
    @JoinColumn(name = "id_guia", referencedColumnName = "id")
    public Guia getGuia() {
        return guia;
    }

    public void setGuia(Guia guia) {
        this.guia = guia;
    }

    @Override
    public String toString() {
        return "cantidad personas: " + cantidad +
                ", precio: " + precio;
    }
}
