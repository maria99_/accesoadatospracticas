package com.mariava.agencia.clases;

import javax.persistence.*;

@Entity
@Table(name = "destino_excursion", schema = "agenciamaria", catalog = "")
public class DestinoExcursion {
    private Long id;
    private Excursion excursion;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_excursion", referencedColumnName = "id", nullable = false)
    public Excursion getExcursion() {
        return excursion;
    }

    public void setExcursion(Excursion excursion) {
        this.excursion = excursion;
    }
}
