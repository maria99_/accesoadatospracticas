package com.mariava.agencia.clases;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Excursion {
    private Long id;
    private String codigo;
    private String nombreExcursion;
    private String descripcion;
    private Double precio;
    private List<Destino> destinos;
    private List<DestinoExcursion> detalles;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre_excursion")
    public String getNombreExcursion() {
        return nombreExcursion;
    }

    public void setNombreExcursion(String nombreExcursion) {
        this.nombreExcursion = nombreExcursion;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Excursion excursion = (Excursion) o;
        return Objects.equals(id, excursion.id) && Objects.equals(codigo, excursion.codigo) && Objects.equals(nombreExcursion, excursion.nombreExcursion) && Objects.equals(descripcion, excursion.descripcion) && Objects.equals(precio, excursion.precio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, nombreExcursion, descripcion, precio);
    }

    @ManyToMany(mappedBy = "excursiones")
    public List<Destino> getDestinos() {
        return destinos;
    }

    public void setDestinos(List<Destino> destinos) {
        this.destinos = destinos;
    }

    @OneToMany(mappedBy = "excursion")
    public List<DestinoExcursion> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DestinoExcursion> detalles) {
        this.detalles = detalles;
    }

    @Override
    public String toString() {
        return nombreExcursion + " - " + precio;
    }
}
